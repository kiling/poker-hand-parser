﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PokerCore;
using PokerCore.HandHistory;
using System.Collections.Generic;

namespace PokerCoreTest.HandHistory
{
    [TestClass]
    public class UndoRedoTest
    {
        List<PokerGame> InitGames()
        {
            var games = new List<PokerGame>();
            games.Add(new PokerGame(100, 0.1f, 0.05f));
            games.Add(new PokerGame(101, 0.1f, 0.05f));
            games.Add(new PokerGame(102, 0.1f, 0.05f));
            games.Add(new PokerGame(103, 0.1f, 0.05f));
            return games;
        }

        [TestMethod]
        public void Test_Undo()
        {
            var games = InitGames();
            var undoredo = new UndoRedo(games);
            undoredo.BeforeDeleteGame(games[1]);
            games.RemoveAt(1);

            Assert.AreEqual(games.Count, 3);
            undoredo.Undo();
            Assert.AreEqual(games.Count, 4);
            Assert.AreEqual(games[3].HandUID, 101);
        }

        [TestMethod]
        public void Test_Redo()
        {
            var games = InitGames();
            var undoredo = new UndoRedo(games);
            undoredo.BeforeDeleteGame(games[2]);
            games.RemoveAt(2);

            undoredo.BeforeDeleteGame(games[1]);
            games.RemoveAt(1);

            undoredo.Undo();
            undoredo.Undo();

            Assert.AreEqual(games.Count, 4);

            undoredo.Redo();
            undoredo.Redo();

            Assert.AreEqual(games.Count, 2);
            Assert.AreEqual(games[0].HandUID, 100);
            Assert.AreEqual(games[1].HandUID, 103);
        }
    }
}
