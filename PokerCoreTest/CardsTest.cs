using Microsoft.VisualStudio.TestTools.UnitTesting;
using PokerCore;

namespace PokerCoreTest
{
    [TestClass]
    public class CardsTest
    {
        [TestMethod]
        public void Create_Card_From_String_9c()
        {
            var card = new Card("9c");
            Assert.AreEqual(card.ToString(), "9c");
        }

        [TestMethod]
        public void Create_Card_From_Long_String_Ad9c()
        {
            Assert.ThrowsException<CardException>(() => new Card("As9c"));
        }

        [TestMethod]
        public void Create_Card_From_Broken_String_Ad9c()
        {
            Assert.ThrowsException<CardException>(() => new Card("9r"));
        }

        [TestMethod]
        public void Card_Comparison()
        {
            var a = new Card("Ad");
            var b = new Card("2c");
            var c = new Card("2h");

            Assert.AreEqual(a > b, true);
            Assert.AreEqual(b < c, true);
        }

        [TestMethod]
        public void Get_Board()
        {
            var board = new Cards();
            Card.GetCards("Ad2c7h7d3s", board);

            Assert.AreEqual(board.Count, 5);
            Assert.AreEqual(board[0].ToString(), "Ad");
            Assert.AreEqual(board[1].ToString(), "2c");
            Assert.AreEqual(board[2].ToString(), "7h");
            Assert.AreEqual(board[3].ToString(), "7d");
            Assert.AreEqual(board[4].ToString(), "3s");
        }
    }
}
