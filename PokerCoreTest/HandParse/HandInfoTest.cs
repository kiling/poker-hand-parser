﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PokerCore.HandsParse;
using System.Collections.Generic;

namespace PokerCoreTest.HandsParse
{
    [TestClass]
    public class HandInfoTest
    {
        [TestMethod]
        public void Parce_HandInfo()
        {
            var hand = new List<string>();
            hand.Add("PokerStars Hand #91821676857:  Hold'em No Limit ($0.10/$0.25 USD) - 2013/01/05 2:20:03 EET [2013/01/04 19:20:03 ET]");
            hand.Add("Table 'Siri VII' 6-max Seat #5 is the button");
            var info = HandParceInfo.Parce(hand);
            Assert.AreEqual(info.BigBlindChips, 0.25, 0.01);
            Assert.AreEqual(info.SmallBlindChips, 0.1, 0.01);
            Assert.AreEqual(info.Room, "PokerStars");
            Assert.AreEqual(info.PokerType, "Hold'em No Limit");
            Assert.AreEqual(info.Limit, "0.10/0.25");
            Assert.AreEqual(info.ButtonSeat, 5);
            Assert.AreEqual(info.Currency, "$");
        }

        [TestMethod]
        public void ButtonSeat_Exeption()
        {
            var hand = new List<string>();
            hand.Add("PokerStars Hand #91821676857:  Hold'em No Limit ($0.10/$0.25 USD) - 2013/01/05 2:20:03 EET [2013/01/04 19:20:03 ET]");
            hand.Add("Table 'Siri VII' 6-max Seat 5 is the button");
            Assert.ThrowsException<ButtonSeatException>(() => HandParceInfo.Parce(hand));
        }


        [TestMethod]
        public void HandInfo_Exception()
        {
            var hand = new List<string>();
            hand.Add("PokerStars Hand #91821676857:  3 ET]");
            hand.Add("Table 'Siri VII' 6-max Seat #5 is the button");
            Assert.ThrowsException<HandInfoException>(() => HandParceInfo.Parce(hand));
        }
    }
}
