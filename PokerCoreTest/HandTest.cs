﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PokerCore;

namespace PokerCoreTest
{
    [TestClass]
    public class HandTest
    {
        [TestMethod]
        public void Create_Hand_From_String_9sQc()
        {
            var card = new Hand("9sQc");
            Assert.AreEqual(card.ToString(), "Qc9s");
        }

        [TestMethod]
        public void Create_Hand_From_Long_String_Ad9cAd()
        {
            Assert.ThrowsException<HandException>(() => new Hand("Ad9cAd"));
        }

        [TestMethod]
        public void Create_Card_From_Broken_String_Aw9c()
        {
            Assert.ThrowsException<CardException>(() => new Hand("Aw9c"));
        }
    }
}
