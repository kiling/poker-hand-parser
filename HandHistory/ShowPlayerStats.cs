﻿using PokerCore;
using PokerCore.Stats;
using System;
using System.Collections.Generic;

namespace StatsGrabber
{
    public class ShowPlayerStats
    {
        public static void ShowHeader()
        {
            var line = String.Format("|{0,20}|{1,7}|{2,7}|{3,7}|{4,7}|{5,7}|{6,7}|{7,7}|",
                "name",
                "hands",
                "vpip",
                "pfr",
                "bet3",
                "wtsd",
                "wonsd",
                "af"
                );
            Console.WriteLine(line);
        }

        public static void ShowStats(PlayerStats playerStats)
        {
            var line = String.Format("|{0,20}|{1,7}|{2,7}|{3,7}|{4,7}|{5,7}|{6,7}|{7,7}|", 
                playerStats.PlayerName, 
                playerStats.TotalHandPlayed.Value,
                playerStats.Preflop.VPIP.PercentToString(false),
                playerStats.Preflop.PFR.PercentToString(false),
                playerStats.Preflop.Bet3B.PercentToString(false),
                playerStats.PostFlop.All.WTSD.PercentToString(false),
                playerStats.PostFlop.All.WonSD.PercentToString(false),
                playerStats.PostFlop.All.AgressionFactor().ToString("0.##")
                );
            Console.WriteLine(line);
        }

        public static void ShowGroup(StatsGroup value, string prefix = "")
        {
            var name = value.Name;
            if (name == "")
                name = value.GetType().Name;
            Console.WriteLine(prefix + name);

            foreach (var item in value.Groups)
                ShowGroup(item, prefix + "\t");

            foreach (var item in value.Values)
                ShowItem(item, prefix + "\t");
        }

        public static void ShowGames(string heroName, List<PokerGame> games, int count)
        {
            Console.WriteLine($"Total player hands : {count}");
            var line = String.Format("|{0,12}|{1,10}|{2,4}|",  $"#", "Board", "Hand");
            Console.WriteLine(line);

            foreach (var game in games)
            {
                var hero = game.PlayersAtTable.Find(player => player.Name == heroName);
                if (hero == null)
                    throw new Exception($"Hero {heroName} not found in game {game.HandUID}");
                line = String.Format("|{0,12}|{1,10}|{2,4}|",
                    $"{game.HandUID}",
                    game.Board.ToString(),
                    hero.Hand?.ToString());
                Console.WriteLine(line);
            }
        }

        static void ShowItem(StatsValue value, string prefix)
        {
            if (value.Possibly == 0)
                return;
            if (value.InPos != null)
                ShowItem(value.InPos, prefix + "\t");
            if (value.OutOfPosition != null)
                ShowItem(value.OutOfPosition, prefix + "\t");
            var hint = value.Hint == "" ? "" : $"\t***{value.Hint}***";
            
            Console.WriteLine($"{prefix}{value.Name} - {value.PercentToString()}{hint}");
        }

        public static void ShowGame(PokerGame game)
        {
            Console.WriteLine();
            Console.WriteLine($"#{game.HandUID} {game.SmallBlind}/{game.BigBlind}");

            for (var i = 0; i < game.PlayersAtTable.Count; i++)
            {
                var player = game.PlayersAtTable[i];
                Console.WriteLine($"\t {player.Position}: {player.Name} ({player.Chips} in chips)");
            }

            Console.WriteLine($"*** {PokerStreet.Preflop} ***");

            var lastStreet = PokerStreet.Preflop;
            for (var i = 0; i < game.Actions.Count; i++)
            {
                var action = game.Actions[i];
                if (lastStreet != action.Street)
                {
                    Console.WriteLine($"*** {action.Street} ***");
                    switch (action.Street)
                    {
                        case PokerStreet.Flop:
                            if (game.Board.Count >= 3)
                                Console.WriteLine($"Board: [{game.Board[0]} {game.Board[1]} {game.Board[2]}]");
                            break;
                        case PokerStreet.Turn:
                            if (game.Board.Count >= 4)
                                Console.WriteLine($"Board: [{game.Board[0]} {game.Board[1]} {game.Board[2]}] [{game.Board[3]}]");
                            break;
                        case PokerStreet.River:
                            if (game.Board.Count >= 5)
                                Console.WriteLine($"Board: [{game.Board[0]} {game.Board[1]} {game.Board[2]} {game.Board[3]}] [{game.Board[4]}]");
                            break;
                    }
                    lastStreet = action.Street;
                }

                var actionStr = $"{action.Player.Name}: ";
                switch (action.ActionType)
                {
                    case PokerActionType.Blind:
                        actionStr = $"posts blind {action.Chips}";
                        break;
                    case PokerActionType.PostsBigBlind:
                    case PokerActionType.PostsSmallBlind:
                    case PokerActionType.PostsBigAndSmallBlind:
                        actionStr += $"posts {action.Chips}";
                        break;
                    case PokerActionType.Ante:
                        actionStr += $"posts the ante {action.Chips}";
                        break;
                    case PokerActionType.Check:
                        actionStr += "checks";
                        break;
                    case PokerActionType.Fold:
                        actionStr += "folds";
                        break;
                    case PokerActionType.Bet:
                        actionStr += $"bet {action.Chips}";
                        break;
                    case PokerActionType.Call:
                        actionStr += $"call {action.Chips}";
                        break;
                    case PokerActionType.Raise:
                        actionStr += $"raise to {action.Chips}";
                        break;
                    case PokerActionType.ShowHand:
                        actionStr += $"show hand";
                        break;
                    case PokerActionType.MucksHand:
                        actionStr += $"mucks hand";
                        break;
                    case PokerActionType.Wins:
                        actionStr += $"wins {action.Chips}";
                        break;
                    case PokerActionType.Wins_Side_Pot:
                        actionStr += $"wins side pot {action.Chips}";
                        break;
                    case PokerActionType.Uncalled_Bet:
                        actionStr += $"uncalled bet {action.Chips}";
                        break;
                }
                Console.WriteLine("\t" + actionStr);
            }
        }
    }
}
