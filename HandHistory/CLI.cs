﻿using PokerCore.HandHistory;
using System;
using System.Collections.Generic;
using System.IO;

namespace StatsGrabber
{
    public class CLI
    {
        HandHistory _handHistory;
        HandHistoryReports _reports;

        public CLI(HandHistory handHistory)
        {
            _handHistory = handHistory;
            _reports = new HandHistoryReports(_handHistory.StatsManager, _handHistory.Games);
        }

        public void LoadHandHistory()
        {
            while (true)
            {
                var handsPath = "";
#if DEBUG
                handsPath = @"C:\projects\handhistories\2013\";
#endif

#if RELEASE
                Console.WriteLine("Enter the path to the directory containing the hand history:");
                handsPath = Console.ReadLine();
#endif

                if (Directory.Exists(handsPath))
                {
                    var errors = _handHistory.LoadHandHisory(handsPath, (index, count, parseHands) => {
                        Console.Write($"Load: {index} files out of {count} \t Games: {parseHands}\r");
                    });

                    Console.Write(new string(' ', Console.WindowWidth));
                    if (errors.Count > 0)
                    {
                        Console.WriteLine("When parsing the hands, the following errors occurred:");
                        foreach (var error in errors)
                            Console.WriteLine($"\t{error}");
                    }

                    ShowTotalHands();
                    UpdateStats();
                    ShowPlayers(new List<string> { "players" });
                    break;
                }
                else
                {
                    Console.WriteLine("Directory not exist");
                }
            }
        }

        void UpdateStats()
        {
            Console.WriteLine("Updating player statistics...");
            _handHistory.UpdateStats();
        }

        void ShowTotalHands()
        {
            Console.WriteLine("Total number of hands history: #" + _handHistory.Games.Count);
        }


        public void Run()
        {
            while (true)
            {
                Console.Write("> ");
                var input = Console.ReadLine();

                var split = input.Split(" ");
                var commands = new List<string>();
                foreach (var command in split)
                {
                    var cmd = command.Trim();
                    if (cmd != "")
                        commands.Add(cmd);
                }

                if (commands.Count > 0)
                {
                    var command = commands[0];
                    if (command == "total")
                    {
                        ShowTotalHands();
                    }
                    else if (command == "player")
                    {
                        PlayerStats(commands);
                    }
                    else if (command == "playerhands")
                    {
                        PlayerHands(commands);
                    }
                    else if (command == "players")
                    {
                        ShowPlayers(commands);
                    }
                    else if (command == "delete")
                    {
                        DeleteGame(commands);
                    }
                    else if (command == "game")
                    {
                        ShowGame(commands);
                    }
                    else if (command == "undo")
                    {
                        Undo(commands);
                    }
                    else if (command == "redo")
                    {
                        Redo(commands);
                    }
                    else if (command == "help")
                    {
                        PrintHelp();
                    }
                    else if (command == "exit")
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine($"{input} - command not found");
                        PrintHelp();
                    }
                }    

            }
        }
        void PlayerHands(List<string> commands)
        {
            var statsManager = _handHistory.StatsManager;

            if (commands.Count >= 2)
            {
                var playerName = commands[1];
                var limit = 10;

                //
                if (commands.Count >= 3)
                {
                    var cmd = commands[2];
                    var isNumeric = int.TryParse(cmd, out int tmp);

                    if (isNumeric)
                    {
                        if (tmp > 0)
                            limit = tmp;
                        else
                        {
                            Console.WriteLine($"Limit must be greater than 0");
                            return;
                        }
                    }
                    else
                    {
                        Console.WriteLine($"Limit must be a number");
                        return;
                    }
                }

                if (statsManager.PlayerStats.ContainsKey(playerName))
                {
                    var (games, count) = _reports.GetPlayerGames(playerName, limit);
                    ShowPlayerStats.ShowGames(playerName, games, count);
                }
                else
                {
                    Console.WriteLine($"Player {playerName} not found!");
                }
            }
            else
                Console.WriteLine("Invalid command parameter entered");
        }

        void PlayerStats(List<string> commands)
        {
            var statsManager = _handHistory.StatsManager;

            if (commands.Count >= 2)
            {
                var playerName = commands[1];
                if (statsManager.PlayerStats.ContainsKey(playerName))
                {
                    var stats = statsManager.GetPlayerStats(playerName);
                    ShowPlayerStats.ShowHeader();
                    ShowPlayerStats.ShowStats(stats);

                    if (commands.Count >= 3)
                    {
                        if (commands[2] == "-all")
                            ShowPlayerStats.ShowGroup(stats);
                        else
                            Console.WriteLine($"Unknown flag {commands[2]}");
                    }
                }
                else
                {
                    Console.WriteLine($"Player {playerName} not found!");
                }
            }
            else
                Console.WriteLine("Invalid command parameter entered");
        }

        void DeleteGame(List<string> commands)
        {
            if (commands.Count >= 2)
            {
                if (long.TryParse(commands[1], out long handUid))
                {
                    if (_handHistory.DeleteGame(handUid))
                    {
                        ShowTotalHands();
                        UpdateStats();
                    }
                    else
                    {
                        Console.WriteLine($"Game delete error {handUid}");
                    }
                }
                else
                {
                    Console.WriteLine($"HandUid must be a number");
                    return;
                }
            }
            else
                Console.WriteLine("Invalid command parameter entered");
        }

        void ShowGame(List<string> commands)
        {
            if (commands.Count >= 2)
            {
                if (long.TryParse(commands[1], out long handUid))
                {
                    var (game, _) = _handHistory.FindGame(handUid);
                    if (game != null)
                    {
                        ShowPlayerStats.ShowGame(game);
                    }
                    else
                    {
                        Console.WriteLine($"Game {handUid} not found");
                    }
                }
                else
                {
                    Console.WriteLine($"HandUid must be a number");
                    return;
                }
            }
            else
                Console.WriteLine("Invalid command parameter entered");
        }

        void Undo(List<string> commands)
        {
            if (_handHistory.UndoAccess)
            {
                _handHistory.Undo();
                ShowTotalHands();
                UpdateStats();
            }
            else
                Console.WriteLine("Undo operation not available");
        }

        void Redo(List<string> commands)
        {
            if (_handHistory.RedoAccess)
            {
                _handHistory.Redo();
                ShowTotalHands();
                UpdateStats();
            }
            else
                Console.WriteLine("Redo operation not available");
        }

        void ShowPlayers(List<string> commands)
        {
            var stats = (SortReportColumn[])Enum.GetValues(typeof(SortReportColumn));

            var sortStats = stats[0];
            var order = SortReportOrder.desc;
            var limit = 10;

            if (commands.Count >= 2)
            {
                var cmd = commands[1];
                if (Enum.TryParse(cmd, out SortReportColumn tmp))
                    sortStats = tmp;
                else
                {
                    Console.WriteLine($"Unknown column to sort {cmd}");
                    return;
                }
            }

            if (commands.Count >= 3)
            {
                var cmd = commands[2];
                if (Enum.TryParse(cmd, out SortReportOrder tmp))
                    order = tmp;
                else
                {
                    Console.WriteLine($"Incorrect sorting parameter entered {cmd}");
                    return;
                }
            }

            if (commands.Count >= 4)
            {
                var cmd = commands[3];
                var isNumeric = int.TryParse(cmd, out int tmp);

                if (isNumeric)
                {
                    if (tmp > 0)
                        limit = tmp;
                    else
                    {
                        Console.WriteLine($"Limit must be greater than 0");
                        return;
                    }
                }
                else
                {
                    Console.WriteLine($"Limit must be a number");
                    return;
                }
            }

            var playerStats = _reports.GetSortedPlayerStats(sortStats, order, limit);
            ShowPlayerStats.ShowHeader();
            foreach (var stat in playerStats)
                ShowPlayerStats.ShowStats(stat);
        }

        void PrintHelp()
        {
            Console.WriteLine();
            //
            Console.WriteLine("The following commands are available:");
            //
            Console.WriteLine("\ttotal - shows total number of hands history ");
            //
            Console.WriteLine("\tplayer [playerName] [-all] - shows stats of the player");
            Console.WriteLine("\t\t[-all] - show all stats");

            //
            Console.WriteLine("\tplayerhands [playerName] [-all] - shows player hands");
            Console.WriteLine("\t\t[limit] - default 10");
            //
            Console.WriteLine("\tplayers [sortStats] [order] [limit] - displays the player table");

            var enums = (SortReportColumn[])Enum.GetValues(typeof(SortReportColumn));
            Console.WriteLine($"\t\t[stats] - default {enums[0]}");
            for (var i = 0; i < enums.Length; i++)
                Console.WriteLine($"\t\t\t{enums[i]}");

            Console.WriteLine("\t\t[order] - default desc");
            Console.WriteLine("\t\t\tasc");
            Console.WriteLine("\t\t\tdesc");

            Console.WriteLine("\t\t[limit] - default 10");
            // 
            Console.WriteLine("\tdelete [handUid] - delete hand history by uid");

            Console.WriteLine("\tshow [handUid] - show hand history by uid");

            Console.WriteLine("\tundo - undo operation");
            Console.WriteLine("\tredo - redo operation");

            Console.WriteLine("\thelp - description of available commands");
            Console.WriteLine("\texit - exits the application");
            Console.WriteLine();
        }
    }
}
