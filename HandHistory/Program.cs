﻿using PokerCore.HandHistory;

namespace StatsGrabber
{
    class Program
    {
        static void Main(string[] args)
        {
            var handHistory = new HandHistory();
            var cli = new CLI(handHistory);
            cli.LoadHandHistory();
            cli.Run();
        }
    }
}