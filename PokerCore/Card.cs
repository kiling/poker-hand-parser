﻿using System;
using System.Collections.Generic;

namespace PokerCore
{
    public class CardException : Exception
    {
        public CardException(string message) : base(message)
        {
        }
    }

    /// <summary>
    /// Класс описывающий одну карту
    /// </summary>
    public class Card
    {
        public Card(string str)
        {
            if (str.Length != 2)
                throw new CardException($"Error parse card from {str}");
            Rank = Array.IndexOf(CardsRank, str[0]);
            Suit = Array.IndexOf(CardsSuit, str[1]);
            Byte = (byte)(Rank * 4 + Suit);
            if (Rank == -1 || Suit == -1)
                throw new CardException($"Error parse card from {str}");
        }

        /// <summary>
        /// Ранг карты
        /// </summary>
        public int Rank { get; private set; }

        /// <summary>
        /// Масть карты
        /// </summary>
        public int Suit { get; private set; }

        public byte Byte { get; private set; }

        public static bool operator >(Card arg1, Card arg2)
        {
            return (arg1.Rank > arg2.Rank) || (arg1.Rank == arg2.Rank && arg1.Suit > arg2.Suit);
        }

        public static bool operator <(Card arg1, Card arg2)
        {
            return arg2 > arg1;
        }

        /// <summary>
        /// В текстовый вид типа Аs
        /// </summary>
        public override string ToString()
        {
            return CardsRank[Rank].ToString() + CardsSuit[Suit].ToString();
        }

        /// <summary>
        /// Массив рангов
        /// </summary>
        public static readonly char[] CardsRank = new char[] { '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A' };

        /// <summary>
        /// Массив мастей
        /// </summary>
        public static readonly char[] CardsSuit = new char[] { 'c', 'd', 'h', 's' };

        public static void GetCards(string cards, Cards list)
        {
            var i = 0;
            while (i < cards.Length - 1)
            {
                var card = cards[i].ToString() + cards[i + 1].ToString();
                i += 2;
                list.Add(new Card(card));
            }
        }
    }

    public class Cards : List<Card>
    {
        public override string ToString()
        {
            var res = "";
            foreach (var card in this)
                res += card.ToString();
            return res;
        }
    }
}
