﻿using System.Collections.Generic;
using System.IO;

namespace PokerCore.HandsParse
{
    public delegate void HandsParseProgress(int progress, int count, int parseHands);

    public class HandsParse
    {
        private int _count = 0;
        private List<string> _parseErrors = new List<string>();
        public List<string> ParseErrors { get => _parseErrors; }

        public HandsParse()
        {
        }

        public void ReadFile(string path, List<PokerGame> games)
        {
            if (File.Exists(path))
            {
                try
                {
                    using (var file = new StreamReader(path))
                    {
                        var hand = new List<string>();
                        string line;
                        while ((line = file.ReadLine()) != null)
                        {
                            if (line.Trim() == "" && hand.Count != 0)
                            {
                                ReadHand(hand, games);
                                hand.Clear();
                            }
                            else if (line != "")
                                hand.Add(line);
                        }
                    }
                }
                catch (IOException e)
                {
                    _parseErrors.Add(e.Message);
                }
            }
            else
            {
                _parseErrors.Add($"File {path} not exists");
            }
        }

        void ReadHand(List<string> hand, List<PokerGame> games)
        {
            try
            {
                var handInfo = HandParceInfo.Parce(hand);
                PokerGame game = new PokerGame(handInfo.HandUid, handInfo.BigBlindChips, handInfo.SmallBlindChips);

                var seatBlock = new SeatBlock(hand, handInfo, game);
                var index = seatBlock.Parse(2);

                var gameBlock = new GameBlock(hand, handInfo, game);
                index = gameBlock.Parse(index);

                var summaryBlock = new SummaryBlock(hand, handInfo, game);
                summaryBlock.Parse(index);

                PrepareChips(game);
                games.Add(game);
            }
            catch (HandReaderException e)
            {
                _parseErrors.Add($"#{_count}: {e.HandLine}: {e.Message}");
            }
        }

        void PrepareChips(PokerGame game)
        {
            var pot = 0.0f;
            foreach (var action in game.Actions)
            {
                if (action.ActionType.IsWins())
                    pot -= action.Chips;
                else
                    pot += action.Chips;
                action.Pot = pot;
            }

            foreach (var player in game.PlayersAtTable)
            {
                var stack = player.Chips;
                foreach (var action in game.Actions)
                {
                    if (action.Player == player)
                    {
                        if (action.ActionType.IsWins())
                            stack += action.Chips;
                        else
                            stack -= action.Chips;
                        action.PlayerStack = stack;
                    }
                }
            }
        }
    }
}
