﻿using System;
using System.Collections.Generic;

namespace PokerCore.HandsParse
{
    public class SummaryBlock
    {
        List<string> _hand;
        HandInfo _handInfo;
        PokerGame _game;

        public SummaryBlock(List<string> hand, HandInfo handInfo, PokerGame game)
        {
            _hand = hand;
            _handInfo = handInfo;
            _game = game;
        }

        public void Parse(int start)
        {
            try
            {
                ParseSummaryBlock(_hand, start);
            }
            catch (HandReaderException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new SummaryBlockException(e.Message);
            }
        }

        void ParseSummaryBlock(List<string> hand, int start)
        {
            var isBlock = false;
            for (var index = start; index < hand.Count; index++)
            {
                var line = hand[index];
                if (line.Contains("*** SUMMARY ***"))
                    isBlock = true;
                else if (isBlock)
                {
                    if (line.Contains("Board ["))
                    {
                        try
                        {
                            var str = line.Replace("FIRST Board [", "").Trim();
                            str = str.Replace("SECOND Board [", "").Trim();
                            str = str.Replace("Board [", "");

                            str = str.Replace("]", "").Trim();
                            var board = str.Replace(" ", "");

                            _game.Board.Clear();
                            Card.GetCards(board, _game.Board);
                            if (_game.Board.Count > 5)
                                throw new ParceBoardException("Error board parse", line);
                        }
                        catch (Exception e)
                        {
                            throw new ParceBoardException(e.Message, line);
                        }
                    }
                }
            }
        }
    }
}