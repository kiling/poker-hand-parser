﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace PokerCore.HandsParse
{
    public class GameBlock
    {
        List<string> _hand;
        HandInfo _handInfo;
        PokerGame _game;

        public GameBlock(List<string> hand, HandInfo handInfo, PokerGame game)
        {
            _hand = hand;
            _handInfo = handInfo;
            _game = game;
        }

        public int Parse(int index)
        {
            try
            {
                index = HoleCardsBlock(_hand, index);
                index = FlopBlock(_hand, index);
                index = TurnBlock(_hand, index);
                index = RiverBlock(_hand, index);
                return ShowDownBlock(_hand, index);
            }
            catch (HandReaderException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new GameBlockException(e.Message);
            }
        }

        void HeroHand(string line)
        {
            try
            {
                foreach (var player in _game.PlayersAtTable)
                {
                    if (line.Contains(player.Name))
                    {
                        line = line.Replace($"Dealt to {player.Name} [", "");
                        line = line.Replace("]", "");
                        var hand = line.Replace(" ", "");
                        if (hand.Length == 4)
                            player.Hand = new Hand(hand);
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                throw new HeroHandException(e.Message, line);
            }
        }

        (Player player, string actionStr) GetPlayerAndActionStr(string line)
        {
            foreach (var player in _game.PlayersAtTable)
            {
                var playerStr = player.Name + ":";
                if (line.Contains(playerStr))
                    return (player, line.Replace(playerStr, "").Trim());
            }
            return (null, "");
        }

        (PokerActionType, float) GetActionAndChips(string actionStr, Player player, PokerStreet street)
        {
            var actionType = PokerActionType.None;
            var chips = 0.0f;
            if (actionStr.Contains("raises"))
            {
                actionType = PokerActionType.Raise;
                actionStr = actionStr.Replace("raises", "").Trim();
                var split = actionStr.Split("to");
                var raiseChips = float.Parse(split[1].Trim(), CultureInfo.InvariantCulture);
                var playerChipsInPot = _game.PlayerChipsInPotInThiStrit(player, street);
                chips = raiseChips - playerChipsInPot;
            }
            else if (actionStr.Contains("bets"))
            {
                actionType = PokerActionType.Bet;
                actionStr = actionStr.Replace("bets", "").Trim();
                var betChips = float.Parse(actionStr, CultureInfo.InvariantCulture);
                chips = betChips;
            }
            else if (actionStr.Contains("calls"))
            {
                actionType = PokerActionType.Call;
                actionStr = actionStr.Replace("calls", "").Trim();
                var callChips = float.Parse(actionStr, CultureInfo.InvariantCulture);
                chips = callChips;
            }
            else if (actionStr.Contains("folds"))
            {
                actionType = PokerActionType.Fold;
                if (actionStr.Trim() != "folds")
                    PlayerShowHand(actionStr, player);
            }
            else if (actionStr.Contains("checks"))
            {
                actionType = PokerActionType.Check;
            }
            else if (actionStr.Contains("shows ["))
            {
                actionType = PokerActionType.ShowHand;
                PlayerShowHand(actionStr, player);
            }
            else if (actionStr.Contains("mucks hand"))
                actionType = PokerActionType.MucksHand;
            else if (actionStr.Contains("doesn't show"))
                actionType = PokerActionType.MucksHand;

            return (actionType, chips);
        }

        void PlayerShowHand(string line, Player player)
        {
            if (line.Contains("shows ["))
                line = line.Replace("shows [", "");
            else if (line.Contains("folds ["))
                line = line.Replace("folds [", "");
            else
                return;
            var split = line.Split("]");
            var hand = split[0].Trim().Replace(" ", "");

            if (hand.Length == 4)
                player.Hand = new Hand(hand);
        }

        void RefundChips(string line, PokerStreet street)
        {
            var playerName = "";
            var chips = 0.0f;
            var actionType = PokerActionType.None;
            if (line.Contains($"Uncalled bet ({_handInfo.Currency}"))
            {
                var str = line.Replace($"Uncalled bet ({_handInfo.Currency}", "");
                var split = str.Split(") returned to");
                playerName = split[1].Trim();
                chips = float.Parse(split[0], CultureInfo.InvariantCulture);
                actionType = PokerActionType.Uncalled_Bet;
            }
            else if (line.Contains($"collected {_handInfo.Currency}"))
            {
                if (line.Contains($"from side pot"))
                    actionType = PokerActionType.Wins_Side_Pot;
                else if (line.Contains($"from main pot") || line.Contains($"from pot"))
                    actionType = PokerActionType.Wins;

                if (actionType != PokerActionType.None)
                {
                    var split = line.Split($"collected {_handInfo.Currency}");
                    playerName = split[0].Trim();
                    split = split[1].Split($"from");
                    var str = split[0].Trim();
                    chips = float.Parse(str, CultureInfo.InvariantCulture);
                }
            }

            if (actionType != PokerActionType.None)
            {
                var action = new PokerAction();
                action.Player = _game.FindPlayer(playerName);
                action.Street = street;
                action.ActionType = actionType;
                action.Chips = chips;
                action.Pot = 0.0f;
                action.PlayerStack = 0.0f;
                _game.Actions.Add(action);
            }
        }

        void AddAction(string line, PokerStreet street)
        {
            try
            {
                var (player, actionStr) = GetPlayerAndActionStr(line);
                if (player != null)
                {
                    actionStr = actionStr.Replace(_handInfo.Currency, "");
                    actionStr = actionStr.Replace("and is all-in", "");
                    var (actionType, chips) = GetActionAndChips(actionStr, player, street);
                    if (actionType != PokerActionType.None)
                    {
                        var action = new PokerAction();
                        action.Player = player;
                        action.Street = street;
                        action.ActionType = actionType;
                        action.Chips = chips;
                        action.Pot = 0.0f;
                        action.PlayerStack = 0.0f;
                        _game.Actions.Add(action);
                    }
                    else
                        throw new AddActionException("Action logic error", line);
                }
                else
                    RefundChips(line, street);
            }
            catch (Exception e)
            {
                throw new AddActionException(e.Message, line);
            }
        }

        int HoleCardsBlock(List<string> hand, int start)
        {
            var isBlock = false;
            int index;
            for (index = start; index < hand.Count; index++)
            {
                var line = hand[index];
                if (line.Contains("*** HOLE CARDS ***"))
                    isBlock = true;
                else
                if (line.Contains("*** FLOP ***") ||
                    line.Contains("*** TURN ***") ||
                    line.Contains("*** RIVER ***") ||
                    line.Contains("*** SHOW DOWN ***") ||
                    line.Contains("*** SUMMARY ***"))
                    break;
                else if (line.Contains("Dealt to "))
                    HeroHand(line);
                else if (isBlock)
                    AddAction(line, PokerStreet.Preflop);
            }

            return index;
        }

        int FlopBlock(List<string> hand, int start)
        {
            var isBlock = false;
            int index;
            for (index = start; index < hand.Count; index++)
            {
                var line = hand[index];
                if (line.Contains("*** FLOP ***"))
                    isBlock = true;
                else
                if (line.Contains("*** TURN ***") ||
                    line.Contains("*** RIVER ***") ||
                    line.Contains("*** SHOW DOWN ***") ||
                    line.Contains("*** SUMMARY ***"))
                    break;
                else if (isBlock)
                    AddAction(line, PokerStreet.Flop);
            }

            return index;
        }

        int TurnBlock(List<string> hand, int start)
        {
            var isBlock = false;
            int index;
            for (index = start; index < hand.Count; index++)
            {
                var line = hand[index];
                if (line.Contains("*** TURN ***"))
                    isBlock = true;
                else
                if (line.Contains("*** RIVER ***") ||
                    line.Contains("*** SHOW DOWN ***") ||
                    line.Contains("*** SUMMARY ***"))
                    break;
                else if (isBlock)
                    AddAction(line, PokerStreet.Turn);
            }

            return index;
        }

        int RiverBlock(List<string> hand, int start)
        {
            var isBlock = false;
            int index;
            for (index = start; index < hand.Count; index++)
            {
                var line = hand[index];
                if (line.Contains("*** RIVER ***"))
                    isBlock = true;
                else
                if (line.Contains("*** SHOW DOWN ***") ||
                    line.Contains("*** SUMMARY ***"))
                    break;
                else if (isBlock)
                    AddAction(line, PokerStreet.River);
            }

            return index;
        }

        int ShowDownBlock(List<string> hand, int start)
        {
            var isBlock = false;
            int index;
            for (index = start; index < hand.Count; index++)
            {
                var line = hand[index];
                if (line.Contains("*** SHOW DOWN ***"))
                    isBlock = true;
                else
                if (line.Contains("*** SUMMARY ***"))
                    break;
                else if (isBlock)
                    AddAction(line, PokerStreet.ShowDown);
            }
            return index;
        }
    }
}
