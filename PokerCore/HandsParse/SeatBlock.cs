﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace PokerCore.HandsParse
{
    public class SeatBlock
    {
        List<string> _hand;
        HandInfo _handInfo;
        PokerGame _game;

        public SeatBlock(List<string> hand, HandInfo handInfo, PokerGame game)
        {
            _hand = hand;
            _handInfo = handInfo;
            _game = game;
        }

        void AddSeatPlayer(string line)
        {
            try
            {
                var split = line.Split(new[] { ':' }, 2);
                var seatStr = split[0].Replace("Seat", "").Trim();
                var seat = Convert.ToInt32(seatStr);

                split = split[1].Split("(" + _handInfo.Currency);
                var playerName = split[0].Trim();

                var playerChipsStr = split[1].Replace(" in chips)", "").Trim();
                var playerChips = float.Parse(playerChipsStr, CultureInfo.InvariantCulture);

                var player = new Player();
                player.Name = playerName;
                player.Chips = playerChips;
                player.Hand = null;
                player.Seat = seat;
                player.Position = Position.None;
                _game.PlayersAtTable.Add(player);
            }
            catch (Exception e)
            {
                throw new SeatPlayerException(e.Message, line);
            }
        }

        void DeterminePlayersPosition(int buttonSeat)
        {
            try
            {
                var btnPlayer = _game.PlayersAtTable.Find(player => player.Seat == buttonSeat);
                var index = _game.PlayersAtTable.IndexOf(btnPlayer);

                if (_game.PlayersAtTable.Count() == 2)
                {
                    foreach (var player in _game.PlayersAtTable)
                        player.Position = btnPlayer == player ? Position.Sb : Position.Bb;
                }
                else
                {
                    var position = Position.Btn;
                    do
                    {
                        var player = _game.PlayersAtTable[index];
                        player.Position = position;
                        position = position.Next();
                        index = (index + 1) % _game.PlayersAtTable.Count;

                    } while (position <= Position.Bb);

                    position = Position.Btn;
                    index = _game.PlayersAtTable.IndexOf(btnPlayer);
                    do
                    {
                        var player = _game.PlayersAtTable[index];
                        player.Position = position;
                        position = position.Prev();
                        index = index - 1;
                        if (index < 0)
                            index = _game.PlayersAtTable.Count - 1;

                    } while (_game.PlayersAtTable[index].Position == Position.None);
                }
            }
            catch (Exception e)
            {
                throw new DeterminePlayersPositionException(e.Message);
            }
        }

        void AddPostsAction(string line, string type)
        {
            try
            {
                var split = line.Split(type);
                var playerName = split[0].Trim();
                var chipsStr = split[1].Replace(_handInfo.Currency, "").Trim();
                var chips = float.Parse(chipsStr, CultureInfo.InvariantCulture);

                var player = _game.FindPlayer(playerName);
                var actionType = PokerActionType.Blind;
                if (type == ": posts the ante")
                    actionType = PokerActionType.Ante;
                else if (type == ": posts small & big blinds")
                    actionType = PokerActionType.PostsBigAndSmallBlind;
                else if (type == ": posts small blind" && player.Position != Position.Sb)
                    actionType = PokerActionType.PostsSmallBlind;
                else if (type == ": posts big blind" && player.Position != Position.Bb)
                    actionType = PokerActionType.PostsBigBlind;

                var action = new PokerAction();
                action.Player = player;
                action.Street = PokerStreet.Preflop;
                action.ActionType = actionType;
                action.Chips = chips;
                action.Pot = 0.0f;
                action.PlayerStack = 0.0f;

                _game.Actions.Add(action);
            }
            catch (Exception e)
            {
                throw new PostsActionExeption(e.Message, line);
            }
        }

        public int Parse(int start)
        {
            var isDeterminePlayersPosition = false;
            Action determinePlayersPosition = () =>
            {
                if (!isDeterminePlayersPosition)
                {
                    isDeterminePlayersPosition = true;
                    DeterminePlayersPosition(_handInfo.ButtonSeat);
                }
            };

            int index = -1;
            try
            {
                for (index = start; index < _hand.Count; index++)
                {
                    var line = _hand[index];

                    if (line == "*** HOLE CARDS ***")
                        break;
                    if (line.Contains("Seat ") && line.Contains("in chips)"))
                    {
                        AddSeatPlayer(line);
                    }
                    else if (line.Contains(": posts small blind"))
                    {
                        determinePlayersPosition();
                        AddPostsAction(line, ": posts small blind");
                    }
                    else if (line.Contains(": posts big blind"))
                    {
                        determinePlayersPosition();
                        AddPostsAction(line, ": posts big blind");
                    }
                    else if (line.Contains(": posts small & big blinds"))
                    {
                        determinePlayersPosition();
                        AddPostsAction(line, ": posts small & big blinds");
                    }
                    else if (line.Contains(": posts the ante"))
                    {
                        determinePlayersPosition();
                        AddPostsAction(line, ": posts the ante");
                    }
                }
            }
            catch (HandReaderException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new SeatPlayerException(e.Message);
            }

            return index;
        }
    }
}
