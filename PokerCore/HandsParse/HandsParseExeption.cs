﻿using System;

namespace PokerCore.HandsParse
{
    public abstract class HandReaderException : Exception
    {
        private readonly string _handLine;

        public HandReaderException(string message, string handLine = "") : base(message)
        {
            _handLine = handLine;
        }

        public string HandLine
        {
            get { return _handLine; }
        }
    }

    public class HandIOException : HandReaderException
    {
        public HandIOException(string message, string handLine = "") : base(message, handLine)
        {
        }
    }

    // HandInfo
    public class HandInfoException : HandReaderException
    {
        public HandInfoException(string message, string handLine = "") : base(message, handLine)
        {
        }
    }

    public class ButtonSeatException : HandReaderException
    {
        public ButtonSeatException(string message, string handLine = "") : base(message, handLine)
        {
        }
    }

    // SeatBlock
    public class DeterminePlayersPositionException : HandReaderException
    {
        public DeterminePlayersPositionException(string message, string handLine = "") : base(message, handLine)
        {
        }
    }

    public class PostsActionExeption : HandReaderException
    {
        public PostsActionExeption(string message, string handLine = "") : base(message, handLine)
        {
        }
    }

    public class SeatPlayerException : HandReaderException
    {
        public SeatPlayerException(string message, string handLine = "") : base(message, handLine)
        {
        }
    }

    public class SeatBlockException : HandReaderException
    {
        public SeatBlockException(string message, string handLine = "") : base(message, handLine)
        {
        }
    }

    // Game Block
    public class AddActionException : HandReaderException
    {
        public AddActionException(string message, string handLine = "") : base(message, handLine)
        {
        }
    }

    public class HeroHandException : HandReaderException
    {
        public HeroHandException(string message, string handLine = "") : base(message, handLine)
        {
        }
    }

    public class GameBlockException : HandReaderException
    {
        public GameBlockException(string message, string handLine = "") : base(message, handLine)
        {
        }
    }

    // SummaryBlock
    public class ParceBoardException : HandReaderException
    {
        public ParceBoardException(string message, string handLine = "") : base(message, handLine)
        {
        }
    }

    public class SummaryBlockException : HandReaderException
    {
        public SummaryBlockException(string message, string handLine = "") : base(message, handLine)
        {
        }
    }
}
