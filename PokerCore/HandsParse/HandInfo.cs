﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace PokerCore.HandsParse
{
    public struct HandInfo
    {
        public string Room;
        public long HandUid;
        public string PokerType;
        public string Limit;
        public string Currency;
        public int ButtonSeat;
        public float BigBlindChips;
        public float SmallBlindChips;
    }

    public class HandParceInfo
    {
        public static HandInfo Parce(List<string> hand)
        {
            var handInfo = GetHandInfo(hand);
            handInfo.ButtonSeat = GetButtonSeat(hand);
            return handInfo;
        }

        static HandInfo GetHandInfo(List<string> hand)
        {
            try
            {
                var line = hand[0];
                HandInfo res;
                res.ButtonSeat = -1;

                // Room
                {
                    var split = line.Split(" Hand #");
                    res.Room = split[0];
                }

                // HandUid
                {
                    var i = line.IndexOf("#", StringComparison.Ordinal);
                    var str = line.Remove(0, i).Trim();
                    var split = str.Split(':');
                    res.HandUid = Convert.ToInt64(split[0].Replace("#", ""));
                }

                {
                    var split = line.Split($"#{res.HandUid}:");
                    var str = split[1].Trim();
                    split = str.Split(") -");
                    str = split[0];
                    split = str.Split(" (");

                    res.PokerType = split[0].Trim();

                    str = split[1].Trim();
                    split = str.Split(" ");
                    res.Limit = split[0].Trim();
                }

                {
                    res.Currency = res.Limit.Substring(0, 1);
                    res.Limit = res.Limit.Replace(res.Currency, "");
                }

                var splitLimit = res.Limit.Split("/");
                res.SmallBlindChips = float.Parse(splitLimit[0], CultureInfo.InvariantCulture);
                res.BigBlindChips = float.Parse(splitLimit[1], CultureInfo.InvariantCulture);

                return res;
            }
            catch (Exception e)
            {
                throw new HandInfoException(e.Message);
            }
        }

        static int GetButtonSeat(List<string> hand)
        {
            try
            {
                var line = hand[1];
                var splitArray = line.Split("#");
                var str = splitArray[1].Replace("is the button", "").Trim();
                return Convert.ToInt32(str);
            }
            catch (Exception e)
            {
                throw new ButtonSeatException(e.Message);
            }
        }
    }
}
