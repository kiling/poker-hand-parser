﻿using System.Collections.Generic;

namespace PokerCore
{
    public enum PokerStreet
    {
        Preflop, Flop, Turn, River, ShowDown
    }

    public static class PokerStreetMethods
    {
        public static bool IsGame(this PokerStreet street)
        {
            return street == PokerStreet.Preflop
                || street == PokerStreet.Flop
                || street == PokerStreet.Turn
                || street == PokerStreet.River;
        }
    }

    public enum PokerActionType
    {
        None, 
        Blind, PostsBigBlind, PostsSmallBlind, PostsBigAndSmallBlind, Ante, 
        Fold, Check, Bet, Call, Raise, ShowHand, MucksHand, 
        Wins, Wins_Side_Pot, Uncalled_Bet
    }

    public static class PokerActionTypeMethods
    {
        public static bool IsGameAction(this PokerActionType action)
        {
            return action == PokerActionType.Fold
                || action == PokerActionType.Check
                || action == PokerActionType.Bet
                || action == PokerActionType.Call
                || action == PokerActionType.Raise;
        }

        public static bool IsBlind(this PokerActionType action)
        {
            return action == PokerActionType.Ante
                || action == PokerActionType.Blind
                || action == PokerActionType.PostsBigBlind
                || action == PokerActionType.PostsSmallBlind
                || action == PokerActionType.PostsBigAndSmallBlind;
        }

        public static bool IsWins(this PokerActionType action)
        {
            return action == PokerActionType.Wins
                || action == PokerActionType.Wins_Side_Pot
                || action == PokerActionType.Uncalled_Bet;
        }

        public static bool MoneyInPot(this PokerActionType action)
        {
            return action == PokerActionType.Bet
                || action == PokerActionType.Call
                || action == PokerActionType.Raise;
        }
    }

    public class PokerAction
    {
        public Player Player { get; set; }
        public PokerStreet Street { get; set; }
        public PokerActionType ActionType { get; set; }
        /// <summary>
        /// Количество денег внесеных в этом действии
        /// </summary>
        public float Chips { get; set; } = 0.0f;
        /// <summary>
        /// Пот c учетом внесенных денег в этом действие
        /// </summary>
        public float Pot { get; set; } = 0.0f;
        /// <summary>
        /// Стек игрока с учетом этого действия
        /// </summary>
        public float PlayerStack { get; set; } = 0.0f;
        public Position Position { get => Player.Position; }
        public int Seat { get => Player.Seat; }
    }

    public class PokerGame
    {
        public long HandUID { get; private set; } = -1;
        public float BigBlind { get; private set; } = -1;
        public float SmallBlind { get; private set; } = -1;
        public List<PokerAction> Actions { get; private set; } = new List<PokerAction>();
        public Cards Board { get; private set; } = new Cards();

        /// <summary>
        /// В текстовый вид типа Аs
        /// </summary>
        public List<Player> PlayersAtTable { get; private set; } = new List<Player>();

        public PokerGame(long handUID, float bigBlind, float smallBlind)
        {
            this.HandUID = handUID;
            this.BigBlind = bigBlind;
            this.SmallBlind = smallBlind;
        }

        public Player FindPlayer(string Name)
        {
            return PlayersAtTable.Find(player => player.Name == Name);
        }

        public float PlayerChipsInPotInThiStrit(Player player, PokerStreet street)
        {
            var result = 0.0f;
            foreach (var action in Actions)
            {
                if (action.Player == player && action.Street == street)
                {
                    if (action.ActionType == PokerActionType.Ante)
                        result += 0.0f;
                    else if (action.ActionType == PokerActionType.PostsBigAndSmallBlind)
                        result += BigBlind;
                    else if (action.ActionType == PokerActionType.PostsBigBlind)
                        result += BigBlind;
                    else if (action.ActionType == PokerActionType.PostsSmallBlind)
                        result += 0.0f;
                    else
                        result += action.Chips;
                }
            }
            return result;
        }
    }
}
