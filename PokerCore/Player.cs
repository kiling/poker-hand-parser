﻿namespace PokerCore
{
    public enum Position
    {
        None, Sb, Bb, Ep, Mp, Co, Btn
    }

    public static class PositionTypeMethods
    {
        public static Position Next(this Position pos)
        {
            var index = (int)pos + 1;
            index %= (int)Position.Btn;
            if (index == 0) index++;
            return (Position)index;
        }

        public static Position Prev(this Position pos)
        {
            var index = (int)pos - 1;
            if (index < 0) index = (int)Position.Btn;
            if (index == 0) index--;
            index %= (int)Position.Btn;
            return (Position)index;
        }
    }

    public class Player
    {
        public string Name { get; set; } = "";
        public Hand Hand { get; set; } = null;
        public float Chips { get; set; } = 0.0f;
        public Position Position { get; set; } = Position.None;
        public int Seat { get; set; } = -1;
    }
}
