﻿using System;
using System.Collections.Generic;

namespace PokerCore.HandHistory
{
    public class UndoRedoException : Exception
    {
        public UndoRedoException(string message) : base(message)
        {
        }
    }

    public abstract class BaseOperation
    {
        public abstract void DoUndo();
        public abstract void DoRedo();
    }

    public abstract class CustomOperation: BaseOperation
    {
        protected PokerGame _copyGame;
        protected long _handUid;
        List<PokerGame> _games; 

        protected void LoadGame(PokerGame copyGame)
        {
            _games.Add(copyGame);
        }

        protected void RemoveGame(long handUid)
        {
            var index = _games.FindIndex(game => game.HandUID == handUid);
            if (index == -1)
                throw new UndoRedoException($"Game {handUid} does not exist in the current list");
            _games.RemoveAt(index);
        }

        public CustomOperation(PokerGame game, List<PokerGame> games)
        {
            // По хорошему объект должен сохраняться в какую либо структуру
            // Например в json
            _copyGame = game;
            _handUid = game.HandUID;
            _games = games;
        }
    }

    public class DeleteOperation : CustomOperation
    {
        public DeleteOperation(PokerGame game, List<PokerGame> games) : base(game, games) { }

        public override void DoUndo()
        {
            LoadGame(_copyGame);
        }
        public override void DoRedo()
        {
            RemoveGame(_handUid);
        }
    }

    public class UndoRedo
    {
        List<PokerGame> _games;
        int _cursor = 0;
        List<BaseOperation> _operations = new List<BaseOperation>();
        public int Cursor { get => _cursor; }
        public int Count { get => _operations.Count; }
        public bool UndoAccess { get => _cursor > 0; }
        public bool RedoAccess { get => _cursor < Count; }

        public UndoRedo(List<PokerGame> games)
        {
            _games = games;
        }

        private void Clear(int cursor)
        {
            _operations.RemoveRange(_cursor, _operations.Count - _cursor);
        }

        private void Add(BaseOperation operation)
        {
            Clear(_cursor);
            _operations.Add(operation);
            _cursor++;
        }

        public void Undo()
        {
            if (_cursor > 0)
            {
                _operations[_cursor - 1].DoUndo();
                _cursor--;
            }
        }

        public void Redo()
        {
            if (_cursor < Count)
            {
                _operations[_cursor].DoRedo();
                _cursor++;
            }
        }

        public void BeforeDeleteGame(PokerGame game)
        {
            Add(new DeleteOperation(game, _games));
        }
    }
}
