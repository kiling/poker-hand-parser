﻿using PokerCore.Stats;
using System;
using System.Collections.Generic;
using System.IO;

namespace PokerCore.HandHistory
{
    public class HandHistory
    {
        List<PokerGame> _games = new List<PokerGame>();
        PlayerStatsManager _statsManager = new PlayerStatsManager();
        UndoRedo _undoRedo;

        public List<PokerGame> Games { get => _games; }
        public PlayerStatsManager StatsManager { get => _statsManager; }

        static void DirSearch(string path, List<string> files)
        {
            try
            {
                foreach (string file in Directory.GetFiles(path))
                {
                    if (file.Contains("No Limit Hold'em.txt"))
                        files.Add(file);
                }

                foreach (string dir in Directory.GetDirectories(path))
                {
                    DirSearch(dir, files);
                }
            }
            catch (System.Exception excpt)
            {
                Console.WriteLine(excpt.Message);
            }
        }

        public HandHistory()
        {
            _undoRedo = new UndoRedo(_games);
        }

        public List<string> LoadHandHisory(string path, HandsParse.HandsParseProgress progress)
        {
            var files = new List<string>();
            DirSearch(path, files);
            _games.Clear();

            var reader = new HandsParse.HandsParse();

            for (var i = 0; i < files.Count; i++)
            {
                reader.ReadFile(files[i], _games);
                progress(i + 1, files.Count, _games.Count);
            }

            return reader.ParseErrors;
        }


        public void UpdateStats()
        {
            _statsManager.Clear();
            foreach (var game in _games)
                foreach (var player in game.PlayersAtTable)
                {
                    var stats = _statsManager.GetPlayerStats(player.Name);
                    stats.StatsGrabber(game, player);
                }
        }

        public (PokerGame, int) FindGame(long handUid)
        {
            var index = _games.FindIndex(game => game.HandUID == handUid);
            if (index > -1)
                return (_games[index], index);
            return (null, -1);
        }

        public bool DeleteGame(long handUid)
        {
            var (game, index) = FindGame(handUid);
            if (game == null)
                return false;
            _undoRedo.BeforeDeleteGame(game);
            _games.RemoveAt(index);
            return true;
        }

        public bool UndoAccess { get => _undoRedo.UndoAccess; }
        public bool RedoAccess { get => _undoRedo.RedoAccess; }

        public void Undo()
        {
            _undoRedo.Undo();
        }

        public void Redo()
        {
            _undoRedo.Redo();
        }
    }
}
