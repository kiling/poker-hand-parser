﻿using PokerCore.Stats;
using System;
using System.Linq;
using System.Collections.Generic;

namespace PokerCore.HandHistory
{
    public enum SortReportColumn
    {
        hands,
        vpip,
        pfr,
        bet3,
    }

    public enum SortReportOrder
    {
        desc,
        asc
    }

    public class HandHistoryReports
    {
        private PlayerStatsManager _statsManager;
        private List<PokerGame> _games;

        public HandHistoryReports(PlayerStatsManager statsManager, List<PokerGame> games)
        {
            _statsManager = statsManager;
            _games = games;
        }

        public List<PlayerStats> GetSortedPlayerStats(SortReportColumn column, SortReportOrder order, int limit)
        {
            List<KeyValuePair<string, PlayerStats>> list = _statsManager.PlayerStats.ToList();
            list.Sort(delegate (KeyValuePair<string, PlayerStats> pair1,
                KeyValuePair<string, PlayerStats> pair2)
            {
                float h1 = 0;
                float h2 = 0;
                switch (column)
                {
                    case SortReportColumn.hands:
                        h1 = pair1.Value.TotalHandPlayed.Value;
                        h2 = pair2.Value.TotalHandPlayed.Value;
                        break;
                    case SortReportColumn.vpip:
                        h1 = pair1.Value.Preflop.VPIP.Percent();
                        h2 = pair2.Value.Preflop.VPIP.Percent();
                        break;
                    case SortReportColumn.pfr:
                        h1 = pair1.Value.Preflop.PFR.Percent();
                        h2 = pair2.Value.Preflop.PFR.Percent();
                        break;
                    case SortReportColumn.bet3:
                        h1 = pair1.Value.Preflop.Bet3B.Percent();
                        h2 = pair2.Value.Preflop.Bet3B.Percent();
                        break;
                    default:
                        throw new Exception($"Unknown column to sort { column }");
                }

                return order == SortReportOrder.desc ? h2.CompareTo(h1) : h1.CompareTo(h2);
            }
            );

            var result = new List<PlayerStats>();
            limit = limit > list.Count() ? list.Count() : limit;
            for (var i = 0; i < limit; i++)
                result.Add(list[i].Value);

            return result;
        }

        public (List<PokerGame>, int) GetPlayerGames(string playerName, int limit)
        {
            var result = new List<PokerGame>();
            var count = 0;
            foreach (var game in _games)
            {
                foreach (var player in game.PlayersAtTable)
                {
                    if (player.Name == playerName)
                    {
                        count++;
                        if (result.Count < limit)
                            result.Add(game);
                        break;
                    }
                }
            }
            return (result, count);
        }
    }
}
