﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PokerCore.Stats
{
    public class PreflopStats : StatsGroup
    {
        public StatsValue VPIP = null;
        public StatsValue PFR = null;
        public StatsValue PF_First_Raise = null;
        public StatsValue Cold_Call = null;
        public StatsValue Cold_Call_Call_To_3Bet = null;
        public StatsValue Cold_Call_Fold_To_3Bet = null;
        public StatsValue Cold_Call_OneCall_OrMore = null;
        public StatsValue Limp = null;
        public StatsValue Limp_Call = null;
        public StatsValue Limp_Fold = null;
        public StatsValue Limp_3Bet = null;
        public StatsValue LWPC = null;
        public StatsValue Call_Bet3B = null;
        public StatsValue Fold_Bet3B = null;
        public StatsValue Raise_Bet3B = null;
        public StatsValue Bet3B = null;
        public StatsValue Squeeze = null;
        public StatsValue FoldToBet4BByPrf = null;
        public StatsValue CallToBet4BByPrf = null;
        public StatsValue RaiseToBet4BByPrf = null;
        public StatsValue Steal = null;
        public StatsValue RaiseSteal = null;

        public PreflopStats() : base() 
        { 
            Add(ref VPIP, "VPIP", "Добровольное вложение денег в пот");
            Add(ref PFR, "PFR", "Количество заходов в банк рейзом на префлопе");
            Add(ref PF_First_Raise, "PF_First_Raise", "Количество заходов в банк рейзом на префлопе когда не было рейзов");
            Add(ref Limp, "Limp", "Колличество коллов в неоткрытом банке на префлопе");
            Add(ref Limp_Call, "Limp_Call", "Колличество коллов рейза после лимпа");
            Add(ref Limp_Fold, "Limp_Fold", "Колличество фолдов рейза после лимпа");
            Add(ref Limp_3Bet, "Limp_3Bet", "Колличество 3bet рейза после лимпа");

            Add(ref LWPC, "LimpedWithPreviousCallers", "Насколько часто игрок часто заходит в банк коллом, после игроков так же вошедших в игру лимпом");

            Add(ref Cold_Call, "Cold_Call", "Количество коллов на 1 рейз соперника на префлопе");
            Add(ref Cold_Call_Call_To_3Bet, "Cold_Call_Call_To_3Bet", "Колдколлер колит 3bet");
            Add(ref Cold_Call_Fold_To_3Bet, "Cold_Call_Fold_To_3Bet", "Колдколлер сбрасывает на 3bet");

            Add(ref Cold_Call_OneCall_OrMore, "Cold_Call_OneCall", "Количество коллов на 1 рейз соперника на префлопе если уже есть колдколлеры");

            Add(ref Bet3B, "Bet3B", "Количество рейзов рейза соперника на префлопе");
            Add(ref Squeeze, "Squeeze", "Сквиз");

            Add(ref Call_Bet3B, "Call_Bet3B", "Количество коллов посе 3betа вашего рейза");
            Add(ref Fold_Bet3B, "Fold_Bet3B", "Количество fold посе 3betа вашего рейза");
            Add(ref Raise_Bet3B, "Raise_Bet3B", "Количество raise посе 3betа вашего рейза");

            // 4bet range вычисляется так: Общий PFR умноженный на 4bet %. 
            // Стоит иметь ввиду, что большинство ситуаций с 4бетом включают поздние позиции,
            // и PFR и 4bet % будут выше общего с поздних позиций. 4бет по позициям на 
            // данный момент отсутствует, и добавление его требует капитальной переработки, 
            // но в будущем он также будет представлен.

            Add(ref FoldToBet4BByPrf, "FoldToBet4BByPrf", "Фолд на 4bet от рейзера");
            Add(ref RaiseToBet4BByPrf, "RaiseToBet4BByPrf", "Фолд на 4bet от рейзера");
            Add(ref CallToBet4BByPrf, "CallToBet4BByPrf", "Колл на 4bet от рейзера");

            Add(ref Steal, "Steal", "Воровство блайдов с позиций CO Btn SB");
            Add(ref RaiseSteal, "RaiseSteal", "Рейз стила");
        }

        public override void StatsGrabber(PokerGame game, Player hero)
        {
            PfrAction(game, hero);
            VpipAction(game, hero);
            LimpAction(game, hero);
            LimpVsRaise(game, hero);
            LWPCAction(game, hero);
            ColdCallVs3Bet(game, hero);
            ColdCallAction(game, hero);
            Bet3BAction(game, hero);
            RaiseStealAction(game, hero);
            StealAction(game, hero);
            CallBet3BAction(game, hero);
            Bet4BAction(game, hero);
            PFFirstRaiseAction(game, hero);
        }

        /// <summary>
        /// Количество заходов в банк рейзом на префлопе когда не было рейзов
        /// </summary>
        private void PFFirstRaiseAction(PokerGame game, Player hero)
        {
            foreach (var action in game.Actions)
            {
                if (action.Street != PokerStreet.Preflop)
                    return;
                if (action.ActionType.IsBlind())
                    continue;
                if (!action.ActionType.IsGameAction())
                    continue;

                if (action.Player != hero && action.ActionType == PokerActionType.Raise)
                    return;
                if (action.ActionType.IsGameAction() && action.Player == hero)
                {
                    var canPFFirstRaise = action.ActionType == PokerActionType.Raise;
                    PF_First_Raise.Inc(canPFFirstRaise, hero.Position);
                    return;
                }
            }
        }

        /// <summary>
        /// На сколько часто игрок заходит в банк рейзом
        /// </summary>
        private void PfrAction(PokerGame game, Player hero)
        {
            foreach (var action in game.Actions)
            {
                if (action.Street != PokerStreet.Preflop)
                    return;
                if (action.ActionType.IsBlind())
                    continue;
                if (!action.ActionType.IsGameAction())
                    continue;

                if (action.ActionType.IsGameAction() && action.Player == hero)
                {
                    var canPfr = action.ActionType == PokerActionType.Raise;
                    PFR.Inc(canPfr, hero.Position);
                    return;
                }
            }
        }

        private void VpipAction(PokerGame game, Player hero)
        {
            foreach (var action in game.Actions)
            {
                if (action.Street != PokerStreet.Preflop)
                    return;
                if (action.ActionType.IsBlind())
                    continue;
                if (!action.ActionType.IsGameAction())
                    continue;

                if (action.ActionType.IsGameAction() && action.Player == hero)
                {
                    var canVpip = action.ActionType.MoneyInPot();
                    VPIP.Inc(canVpip, hero.Position);
                    return;
                }
            }
        }

        private void LimpVsRaise(PokerGame game, Player hero)
        {
            // Игрок уже совершил лимп
            // После лимпа игрока был рейз
            // Смотрим как игрок отреагировал Limp/Fold Limp/Call Limp/3Bet
            if (hero.Position == Position.Bb)
                return;

            int playerAction = 0, playerCall = 0, otherRaise = 0;
            foreach (var action in game.Actions)
            {
                if (action.Street != PokerStreet.Preflop)
                    return;
                if (action.ActionType.IsBlind())
                    continue;
                if (!action.ActionType.IsGameAction())
                    continue;

                // Если не было рейза и игрок сделал call
                if (action.Player == hero)
                {
                    if (otherRaise == 1 && playerCall == 1)
                    {
                        Limp_Call.Inc(action.ActionType == PokerActionType.Call, hero.Position);
                        Limp_Fold.Inc(action.ActionType == PokerActionType.Fold, hero.Position);
                        Limp_3Bet.Inc(action.ActionType == PokerActionType.Raise, hero.Position);
                        return;
                    }

                    if (action.ActionType.IsGameAction())
                    {
                        if (action.ActionType == PokerActionType.Call)
                            playerCall++;
                        playerAction++;
                        if (playerCall == 0 || playerAction > 1)
                            return;
                    }
                }
                else
                {
                    if (action.ActionType == PokerActionType.Raise)
                        otherRaise++;
                    if (otherRaise > 1)
                        return;
                }
            }
        }

        /// <summary>
        /// Уравнивание ставки, когда в банк еще никто не зашел  
        /// </summary>
        private void LimpAction(PokerGame game, Player hero)
        {
            if (hero.Position == Position.Bb)
                return;
            foreach (var action in game.Actions)
            {
                if (action.Street != PokerStreet.Preflop)
                    return;
                if (action.ActionType.IsBlind())
                    continue;
                if (!action.ActionType.IsGameAction())
                    continue;

                // Ищим действие
                if (action.Player != hero && action.ActionType == PokerActionType.Raise)
                    return;
                //Рейз не был сделан и очередь дошла до игрока 
                if (action.Player == hero && action.ActionType.IsGameAction())
                {
                    var canLimp = action.ActionType == PokerActionType.Call;
                    Limp.Inc(canLimp, hero.Position);
                    return;
                }
            }
        }

        /// <summary>
        /// Насколько часто игрок часто заходит в банк
        /// коллом, после игроков так же вошедших в
        /// игру лимпом
        /// </summary>
        private void LWPCAction(PokerGame game, Player hero)
        {
            if (hero.Position == Position.Bb)
                return;
            var limp = 0;
            foreach (var action in game.Actions)
            {
                if (action.Street != PokerStreet.Preflop)
                    return;
                if (action.ActionType.IsBlind())
                    continue;
                if (!action.ActionType.IsGameAction())
                    continue;
                if (action.Player != hero && action.ActionType == PokerActionType.Raise)
                    return;
                if (action.Player != hero && action.ActionType == PokerActionType.Call)
                    limp++;

                if (action.ActionType.IsGameAction() && action.Player == hero)
                {
                    if (limp > 0 && action.Player.Position != Position.Bb)
                    {
                        var canLWPC = action.ActionType == PokerActionType.Call;
                        LWPC.Inc(canLWPC, hero.Position);
                        return;
                    }
                }
            }
        }

        private void ColdCallVs3Bet(PokerGame game, Player hero)
        {
            // Игрок уже совершил лимп
            // После лимпа игрока был рейз
            // Смотрим как игрок отреагировал Limp/Fold Limp/Call Limp/3Bet
            if (hero.Position == Position.Bb)
                return;
            var pos3Bet = Position.Sb;
            int playerAction = 0, playerCall = 0, otherRaise = 0;
            foreach (var action in game.Actions)
            {
                if (action.Street != PokerStreet.Preflop)
                    return;
                if (action.ActionType.IsBlind())
                    continue;
                if (!action.ActionType.IsGameAction())
                    continue;

                // Если не было рейза и игрок сделал call
                if (action.Player == hero)
                {
                    // Есть 3Bet и игрок делает ColdCall3Bet
                    if (otherRaise == 2 && playerCall == 1)
                    {
                        var inPos = HeroInPos(hero.Position, pos3Bet, game);
                        Cold_Call_Call_To_3Bet.Inc(action.ActionType == PokerActionType.Call, hero.Position, inPos);
                        Cold_Call_Fold_To_3Bet.Inc(action.ActionType == PokerActionType.Fold, hero.Position, inPos);
                        return;
                    }

                    // Если есть один рейз и игрок делает ColdCall
                    if (otherRaise == 1 && action.ActionType.IsGameAction())
                    {
                        if (action.ActionType == PokerActionType.Call)
                            playerCall++;
                        playerAction++;
                        if (playerCall == 0 || playerAction > 1)
                            return;
                    }
                }
                else
                {
                    if (action.ActionType.IsGameAction() && action.ActionType == PokerActionType.Raise)
                    {
                        otherRaise++;
                        pos3Bet = action.Position;
                    }
                    if (otherRaise > 2)
                        return;
                }
            }
        }

        private void ColdCallAction(PokerGame game, Player hero)
        {
            // Закколировал Рейз в холодную
            // С учетом что до этго не вкладывал деньги
            // если Limp Raise Coll -> Limp/Call
            // ColdCall только одного Raise
            // Если 2 Raise то это уже ColdCall3Bet

            var raise = 0;
            var call = 0;
            Position raisePos = Position.Sb;
            var playerAction = 0;
            foreach (var action in game.Actions)
            {
                if (action.Street != PokerStreet.Preflop)
                    return;
                if (action.ActionType.IsBlind())
                    continue;
                if (!action.ActionType.IsGameAction())
                    continue;

                if (raise == 1 && playerAction == 0 && action.Player == hero)
                {
                    var canColdCall = action.ActionType == PokerActionType.Call;
                    var inPos = HeroInPos(hero.Position, raisePos, game);
                    Cold_Call.Inc(canColdCall, hero.Position, inPos);
                    if (call > 0)
                        Cold_Call_OneCall_OrMore.Inc(canColdCall, hero.Position, inPos);
                    return;
                }

                if (action.Player == hero)
                    playerAction++;
                else
                {
                    if (action.ActionType == PokerActionType.Raise)
                    {
                        raise++;
                        raisePos = action.Position;
                    }
                    else if (action.ActionType == PokerActionType.Call && raise == 1)
                        call++;
                }
            }
        }

        private void Bet3BAction(PokerGame game, Player hero)
        {
            Position raisePos = Position.Sb;
            var raise = 0;
            var call = 0;
            var heroAction = 0;
            foreach (var action in game.Actions)
            {
                if (action.Street != PokerStreet.Preflop)
                    return;
                if (action.ActionType.IsBlind())
                    continue;
                if (!action.ActionType.IsGameAction())
                    continue;

                if (raise == 1 && action.Player == hero && heroAction == 0)
                {
                    var can3Bet = action.ActionType == PokerActionType.Raise;
                    var inPos = HeroInPos(hero.Position, raisePos, game);
                    Bet3B.Inc(can3Bet, hero.Position, inPos);
                    if (call >= 1)
                        Squeeze.Inc(can3Bet, hero.Position, inPos);
                    return;
                }
                if (action.Player == hero && action.ActionType.IsGameAction())
                    heroAction++;
                if (action.Player != hero)
                    if (action.ActionType == PokerActionType.Raise)
                    {
                        raisePos = action.Position;
                        raise++;
                    }
                    else if (action.ActionType == PokerActionType.Call && raise == 1)
                        call++;
            }
        }

        private void RaiseStealAction(PokerGame game, Player hero)
        {
            if (hero.Position == Position.Bb || hero.Position == Position.Sb || hero.Position == Position.Bb)
            {
                var raiseSteal = 0;
                var raise = 0;
                var raisePos = Position.Sb;

                foreach (var action in game.Actions)
                {
                    if (action.Street != PokerStreet.Preflop)
                        return;
                    if (action.ActionType.IsBlind())
                        continue;
                    if (!action.ActionType.IsGameAction())
                        continue;

                    if (raise == 1 && raiseSteal == 1 && action.Player == hero)
                    {
                        var canRaiseSteal = action.ActionType == PokerActionType.Raise;
                        RaiseSteal.Inc(canRaiseSteal, hero.Position, HeroInPos(hero.Position, raisePos, game));
                        return;
                    }

                    if (action.Player != hero)
                    {
                        if (action.ActionType == PokerActionType.Raise)
                        {
                            if (action.Player.Position == Position.Co ||
                                action.Player.Position == Position.Btn ||
                                action.Player.Position == Position.Sb)
                            {
                                raiseSteal++;
                                raisePos = action.Player.Position;
                            }
                            raise++;
                        }
                        else if (action.ActionType == PokerActionType.Call)
                            return;
                    }
                }
            }
        }

        private void StealAction(PokerGame game, Player hero)
        {
            //Если до тебя вообще никто не вошел в банк
            //и ты на позиции Co Btn или Sb
            foreach (var action in game.Actions)
            {
                if (action.Street != PokerStreet.Preflop)
                    return;
                if (action.ActionType.IsBlind())
                    continue;
                if (!action.ActionType.IsGameAction())
                    continue;

                if (action.Player != hero &&
                    (action.ActionType == PokerActionType.Call || action.ActionType == PokerActionType.Raise))
                    return;

                if (action.ActionType.IsGameAction() && action.Player == hero)
                {
                    var canSteal = action.ActionType == PokerActionType.Raise;
                    if (hero.Position == Position.Co || hero.Position == Position.Btn || hero.Position == Position.Sb)
                        Steal.Inc(canSteal, hero.Position);
                    return;
                }
            }
        }

        private void CallBet3BAction(PokerGame game, Player hero)
        {
            // Если hero сделал рейз
            // И на его рейз оппонент делает 3bet
            bool heroFirstRaise = false;
            int allRaise = 0;

            Position raisePos = Position.Sb;
            foreach (var action in game.Actions)
            {
                if (action.Street != PokerStreet.Preflop)
                    return;
                if (action.ActionType.IsBlind())
                    continue;
                if (!action.ActionType.IsGameAction())
                    continue;

                if (heroFirstRaise && allRaise == 2 && action.Player == hero)
                {
                    var isCall = action.ActionType == PokerActionType.Call;
                    var isFold = action.ActionType == PokerActionType.Fold;
                    var isRaise = action.ActionType == PokerActionType.Raise;

                    var inPos = HeroInPos(hero.Position, raisePos, game);
                    Call_Bet3B.Inc(isCall, hero.Position, inPos);
                    Fold_Bet3B.Inc(isFold, hero.Position, inPos);
                    Raise_Bet3B.Inc(isRaise, hero.Position, inPos);
                    return;
                }

                if (action.ActionType == PokerActionType.Raise)
                {
                    if (action.Player == hero)
                    {
                        if (allRaise == 0)
                            heroFirstRaise = true;
                        else
                            return;
                    }
                    else
                        raisePos = action.Position;
                    allRaise++;
                }
            }
        }

        private void Bet4BAction(PokerGame game, Player hero)
        {
            // Hero слелал 3bet и упал на 4bet

            var otherRaise = 0;
            var heroRaise = 0;

            Player pfr = null;
            foreach (var action in game.Actions)
            {
                if (action.Street != PokerStreet.Preflop)
                    return;
                if (action.ActionType.IsBlind())
                    continue;
                if (!action.ActionType.IsGameAction())
                    continue;

                if (action.Player == hero && pfr != null && heroRaise == 1 && otherRaise == 2)
                {
                    FoldToBet4BByPrf.Inc(action.ActionType == PokerActionType.Fold, hero.Position);
                    RaiseToBet4BByPrf.Inc(action.ActionType == PokerActionType.Raise, hero.Position);
                    CallToBet4BByPrf.Inc(action.ActionType == PokerActionType.Call, hero.Position);
                    break;
                }

                if (action.ActionType == PokerActionType.Raise)
                {
                    if (action.Player == hero)
                    {
                        if (otherRaise == 1)
                        {
                            heroRaise++;// 3Bet
                        }
                        else
                            break;
                    }
                    else
                    {
                        if (heroRaise == 0)
                        {
                            otherRaise++; // PFR
                            pfr = action.Player;
                        }
                        else if (pfr == action.Player && heroRaise == 1)
                        {
                            otherRaise++; // 4bet
                        }
                        else
                            break;
                    }
                }
            }
        }
    }


}
