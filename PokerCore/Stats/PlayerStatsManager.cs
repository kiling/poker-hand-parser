﻿using System.Collections.Generic;

namespace PokerCore.Stats
{
    public class PlayerStatsManager
    {
        private Dictionary<string, PlayerStats> _playerStats = new Dictionary<string, PlayerStats>();
        public Dictionary<string, PlayerStats> PlayerStats { get => _playerStats; }

        public PlayerStats GetPlayerStats(string playerName)
        {
            if (_playerStats.ContainsKey(playerName))
            {
                var result = _playerStats[playerName];
                return result;
            }
            else
            {
                var stats = new PlayerStats(playerName);
                _playerStats.Add(playerName, stats);
                return stats;
            }
        }

        public void Clear()
        {
            _playerStats.Clear();
        }
    }
}
