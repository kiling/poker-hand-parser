﻿using System;
using System.Collections.Generic;
using StatsType = System.UInt32;

namespace PokerCore.Stats
{
    public class PlayerStatsExeption : Exception
    {
        public PlayerStatsExeption(string message) : base(message)
        {
        }
    }

    public class StatsResult
    {
        public StatsResult(bool isAction, Position position)
        {
            this.Position = position;
            this.IsAction = isAction;
        }
        public Position Position { get; private set; } = Position.Bb;
        public bool IsAction { get; private set; } = false;
    }

    public class StatsValue
    {
        public string Name { get; private set; } = null;
        public string Hint { get; private set; } = null;
        public StatsType Value { get; private set; } = 0;
        public StatsType Possibly { get; private set; } = 0;
        public StatsType Value_Ep { get; private set; } = 0;
        public StatsType Possibly_Ep { get; private set; } = 0;
        public StatsType Value_Mp { get; private set; } = 0;
        public StatsType Possibly_Mp { get; private set; } = 0;
        public StatsType Value_Co { get; private set; } = 0;
        public StatsType Possibly_Co { get; private set; } = 0;
        public StatsType Value_Btn { get; private set; } = 0;
        public StatsType Possibly_Btn { get; private set; } = 0;
        public StatsType Value_Sb { get; private set; } = 0;
        public StatsType Possibly_Sb { get; private set; } = 0;
        public StatsType Value_Bb { get; private set; } = 0;
        public StatsType Possibly_Bb { get; private set; } = 0;
        public StatsValue InPos { get; private set; } = null;
        public StatsValue OutOfPosition { get; private set; } = null;

        public void Inc(bool incValue, Position position, bool? inPos = null)
        {
            if (inPos != null)
                if (inPos.Value)
                {
                    if (InPos == null)
                        InPos = new StatsValue("IP", "");
                    InPos.Inc(incValue, position);
                }
                else
                {
                    if (OutOfPosition == null)
                        OutOfPosition = new StatsValue("OOP", "");
                    OutOfPosition.Inc(incValue, position);
                }

            if (incValue && Value < StatsType.MaxValue)
                Value++;
            if (Possibly < StatsType.MaxValue)
                Possibly++;

            switch (position)
            {
                case Position.Ep:
                    if (incValue && Value_Ep < StatsType.MaxValue)
                        Value_Ep++;
                    if (Possibly_Ep < StatsType.MaxValue)
                        Possibly_Ep++;
                    break;
                case Position.Mp:
                    if (incValue && Value_Mp < StatsType.MaxValue)
                        Value_Mp++;
                    if (Possibly_Mp < StatsType.MaxValue)
                        Possibly_Mp++;
                    break;
                case Position.Co:
                    if (incValue && Value_Co < StatsType.MaxValue)
                        Value_Co++;
                    if (Possibly_Co < StatsType.MaxValue)
                        Possibly_Co++;
                    break;
                case Position.Btn:
                    if (incValue && Value_Btn < StatsType.MaxValue)
                        Value_Btn++;
                    if (Possibly_Btn < StatsType.MaxValue)
                        Possibly_Btn++;
                    break;
                case Position.Sb:
                    if (incValue && Value_Sb < StatsType.MaxValue)
                        Value_Sb++;
                    if (Possibly_Sb < StatsType.MaxValue)
                        Possibly_Sb++;
                    break;
                case Position.Bb:
                    if (incValue && Value_Bb < StatsType.MaxValue)
                        Value_Bb++;
                    if (Possibly_Bb < StatsType.MaxValue)
                        Possibly_Bb++;
                    break;
            }
        }

        public StatsValue()
        {
            this.Name = "";
            this.Hint = "";
        }

        public StatsValue(string name, string hint)
        {
            this.Name = name;
            this.Hint = hint;
        }

        public override string ToString()
        {
            var percent = (100.0f * (float)Value / (float)Possibly).ToString("0.##");
            return $"{Name} {percent}% ({Possibly})";
        }

        public float Percent()
        {
            if (Possibly == 0)
                return 0;
            return (float)Value / (float)Possibly;
        }

        public string PercentToString(bool showPossibly = true)
        {
            if (Possibly == 0)
                return "-";
            var percent = 100.0f * (float)Value / (float)Possibly;

            if (showPossibly)
                return $"{percent.ToString("0.00")}%/({Possibly})";
            return $"{percent.ToString("0.00")}%";
        }

        public float PercentByPos(Position pos)
        {
            switch (pos)
            {
                case Position.Sb:
                    if (Possibly_Sb == 0)
                        return 0;
                    return (float)Value_Sb / (float)Possibly_Sb;
                case Position.Bb:
                    if (Possibly_Bb == 0)
                        return 0;
                    return (float)Value_Bb / (float)Possibly_Bb;
                case Position.Ep:
                    if (Possibly_Ep == 0)
                        return 0;
                    return (float)Value_Ep / (float)Possibly_Ep;
                case Position.Mp:
                    if (Possibly_Mp == 0)
                        return 0;
                    return (float)Value_Mp / (float)Possibly_Mp;
                case Position.Co:
                    if (Possibly_Co == 0)
                        return 0;
                    return (float)Value_Co / (float)Possibly_Co;
                case Position.Btn:
                    if (Possibly_Btn == 0)
                        return 0;
                    return (float)Value_Btn / (float)Possibly_Btn;
            }
            return 0;
        }
    }

    public class StatsGroup
    {
        public List<StatsValue> Values { get; private set; } = new List<StatsValue>();
        public List<StatsGroup> Groups { get; private set; } = new List<StatsGroup>();
        public string Name { get; set; } = "";

        public void Add(ref StatsValue outValue, string name, string hint)
        {
            if (outValue == null)
                outValue = new StatsValue(name, hint);
            Values.Add(outValue);
        }

        public StatsGroup()
        {
        }

        public virtual void StatsGrabber(PokerGame game, Player hero)
        {
        }

        public static bool HeroInPos(Position hero, Position enemy, PokerGame game)
        {
            if (game.PlayersAtTable.Count == 2)
            {
                if ((hero == Position.Sb && enemy == Position.Bb) || (hero == Position.Bb && enemy == Position.Sb))
                {
                    return (int)hero < (int)enemy;
                }
                else
                    throw new PlayerStatsExeption("Invalid positions");
            }
            return (int)hero > (int)enemy;
        }
    }

    public class PlayerStats : StatsGroup
    {
        public string PlayerName { get; set; } = "";
        public StatsValue TotalHandPlayed = null;
        public PreflopStats Preflop = null;
        public FlopStats Flop = null;
        public PostFlopStats PostFlop = null;

        public PlayerStats(string playerName) : base()
        {
            PlayerName = playerName;
            Add(ref TotalHandPlayed, "TotalHandPlayed", "Количесто рук");
            Preflop = new PreflopStats();
            Groups.Add(Preflop);
            Flop = new FlopStats();
            Groups.Add(Flop);
            PostFlop = new PostFlopStats();
            Groups.Add(PostFlop);
        }

        public override void StatsGrabber(PokerGame game, Player hero)
        {
            TotalHandPlayed.Inc(true, hero.Position);
            Preflop.StatsGrabber(game, hero);
            Flop.StatsGrabber(game, hero);
            PostFlop.StatsGrabber(game, hero);
        }
    }
}
