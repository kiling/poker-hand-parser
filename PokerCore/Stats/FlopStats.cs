﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PokerCore.Stats
{
    public class FlopStatsBase : StatsGroup
    {
        // Preflop Raiser
        public StatsValue CBet = null;
        // Preflop Raiser IP
        public StatsValue Raise_DonkBetMin = null;
        public StatsValue Raise_DonkBet = null;
        // Preflop Raiser OOP
        public StatsValue Check_And_Fold = null;
        public StatsValue Check_And_Call = null;
        public StatsValue Check_And_Raise = null;
        // Preflp Caller
        // IP
        public StatsValue Bet_To_CheckPreflopAgg = null;
        public StatsValue Check_To_CheckPreflopAgg = null;

        public StatsValue Fold_To_Cbet = null;
        public StatsValue Call_To_Cbet = null;
        public StatsValue Raise_To_Cbet = null;
        public StatsValue DonkBetMin = null;
        public StatsValue DonkBet = null;
        // OOP
        public StatsValue Check_And_Fold_To_Cbet = null;
        public StatsValue Check_And_Call_To_Cbet = null;
        public StatsValue Check_And_Raise_To_Cbet = null;

        public FlopStatsBase() : base() 
        {
            // Preflop Raiser
            Add(ref CBet, "PA-CBet", "Насколько часто ставит продолженую ставку");
            // Preflop Raiser IP
            Add(ref Raise_DonkBetMin, "PA-Raise_DonkBetMin", "Рейз с малой ставкой <= 0.2 Банка");
            Add(ref Raise_DonkBet, "PA-Raise_DonkBet", "Рейз донкбета");
            // Preflop Raiser OOP
            Add(ref Check_And_Fold, "PA-OOP_Check_And_Fold", "Чек/фолд от префлоп агрессора");
            Add(ref Check_And_Call, "PA-OOP_Check_And_Call", "Чек/колл от префлоп агрессора");
            Add(ref Check_And_Raise, "PA-OOP_Check_And_Raise", "Чек/рейз от префлоп агрессора");

            // Preflp Caller
            // IP
            Add(ref Bet_To_CheckPreflopAgg, "PC-Bet_To_CheckPreflopAgg", "Бет в ответ на чек от префлоп агрессора");
            Add(ref Check_To_CheckPreflopAgg, "PC-Check_To_CheckPreflopAgg", "Check в ответ на чек от префлоп агрессора");

            Add(ref Fold_To_Cbet, "PC-Fold_To_CBet", "Насколько часто сбрасывает на продолженую ставку");
            Add(ref Call_To_Cbet, "PC-Call_To_Cbet", "Насколько часто колит продолженую ставку");
            Add(ref Raise_To_Cbet, "PC-Raise_To_Cbet", "Насколько часто рейзит продолженую ставку");
            Add(ref DonkBetMin, "PC-DonkBetMin", "Донкбет с малой ставкой <= 0.2 Банка");
            Add(ref DonkBet, "PC-DonkBet", "Донкбет");
            // OOP
            Add(ref Check_And_Fold_To_Cbet, "PC-Check_And_Fold_To_Cbet", "Check и Fold На Cbet");
            Add(ref Check_And_Call_To_Cbet, "PC-Check_And_Call_To_Cbet", " Check и Call На Cbet");
            Add(ref Check_And_Raise_To_Cbet, "PC-Check_And_Raise_To_Cbet", "Check и Raise На Cbet");
        }
    }

    public class FlopStats : StatsGroup
    {
        public FlopStatsBase All { get; private set; } = null;
        public FlopStatsBase OneRaisePot_HeadsUp { get; private set; } = null;
        public FlopStatsBase OneRaisePot_MultiPot { get; private set; } = null;
        public FlopStatsBase Bet3Pot { get; private set; } = null;

        public FlopStats() : base() 
        {
            if (All == null)
                All = new FlopStatsBase();
            All.Name = "All";
            Groups.Add(All);

            if (OneRaisePot_HeadsUp == null)
                OneRaisePot_HeadsUp = new FlopStatsBase();
            OneRaisePot_HeadsUp.Name = "OneRaisePot_HeadsUp";
            Groups.Add(OneRaisePot_HeadsUp);

            if (OneRaisePot_MultiPot == null)
                OneRaisePot_MultiPot = new FlopStatsBase();
            OneRaisePot_MultiPot.Name = "OneRaisePot_MultiPot";
            Groups.Add(OneRaisePot_MultiPot);

            if (Bet3Pot == null)
                Bet3Pot = new FlopStatsBase();
            Bet3Pot.Name = "Bet3Pot";
            Groups.Add(Bet3Pot);
        }

        public override void StatsGrabber(PokerGame game, Player hero)
        {
            HeroIsPreflopAgg(game, hero);
            HeroIsNoPreflopAgg(game, hero);
        }

        private FlopStatsBase GetActualFlopStats(PreflopActionStatsType preflopActions)
        {
            FlopStatsBase flopType = null;
            if (preflopActions.PotType == PreflopActionPotType.RaisePot)
            {
                if (preflopActions.PlayersInGame == 2)
                    flopType = OneRaisePot_HeadsUp;
                else
                    flopType = OneRaisePot_MultiPot;
            }
            else if (preflopActions.PotType == PreflopActionPotType.Bet3Pot)
                flopType = Bet3Pot;
            return flopType;
        }

        private void HeroIsPreflopAgg(PokerGame game, Player hero)
        {
            var preflopActions = PostFlopStats.GetPreflopActions(game);
            if (preflopActions == null || preflopActions.PotType == PreflopActionPotType.LimpPot)
                return;
            if (preflopActions.PreflopAggressor != hero)
                return;

            var flopStartPot = -1.0f;
            var raiseAction = 0;
            var callAction = 0;
            var lastBet = 0.0f;
            var lastHeroAction = PokerActionType.None;

            foreach (var action in game.Actions)
            {
                if (action.Street != PokerStreet.Flop)
                    continue;
                if (!action.ActionType.IsGameAction())
                    continue;
                if (flopStartPot < 0.0f)
                    flopStartPot = action.Pot;
                var isBet = action.ActionType == PokerActionType.Bet;
                var isCall = action.ActionType == PokerActionType.Call;
                var isRaise = action.ActionType == PokerActionType.Raise;
                var isFold = action.ActionType == PokerActionType.Fold;
                var flopType = GetActualFlopStats(preflopActions);

                if (action.Player == hero && preflopActions.PreflopAggressor == hero)
                {
                    // Hero префлоп агрессор Preflop Agg (PA)
                    if (raiseAction == 0 && lastHeroAction == PokerActionType.None)
                    {
                        // * PreflopAgg IP and OOP
                        // -CBet
                        All.CBet.Inc(isBet, hero.Position, preflopActions.PreflopAggInPos);
                        flopType?.CBet.Inc(isBet, hero.Position, preflopActions.PreflopAggInPos);
                    }
                    else if (raiseAction == 1)
                    {
                        if (lastHeroAction == PokerActionType.Check)
                        {
                            All.Check_And_Fold.Inc(isFold, hero.Position);
                            All.Check_And_Call.Inc(isCall, hero.Position);
                            All.Check_And_Raise.Inc(isRaise, hero.Position);
                            flopType?.Check_And_Fold.Inc(isFold, hero.Position);
                            flopType?.Check_And_Call.Inc(isCall, hero.Position);
                            flopType?.Check_And_Raise.Inc(isRaise, hero.Position);
                        }
                        else if (lastHeroAction == PokerActionType.None)
                        {
                            // - Raise DonkBet
                            if (lastBet <= 0.2f * flopStartPot)
                            {
                                All.Raise_DonkBetMin.Inc(isRaise, hero.Position);
                                flopType?.Raise_DonkBetMin.Inc(isRaise, hero.Position);
                            }
                            All.Raise_DonkBet.Inc(isRaise, hero.Position);
                            flopType?.Raise_DonkBet.Inc(isRaise, hero.Position);
                        }
                    }
                    else
                    {
                        // - Preflop3Bet или Preflop4Bet
                    }

                    lastHeroAction = action.ActionType;
                }
                else
                {
                    if (action.ActionType == PokerActionType.Bet || action.ActionType == PokerActionType.Raise)
                    {
                        raiseAction++;
                        lastBet = action.Chips;
                    }
                    if (action.ActionType == PokerActionType.Call)
                        callAction++;
                }
            }
        }

        private void HeroIsNoPreflopAgg(PokerGame game, Player hero)
        {
            var preflopActions = PostFlopStats.GetPreflopActions(game);
            if (preflopActions == null || preflopActions.PotType == PreflopActionPotType.LimpPot)
                return;
            if (preflopActions.PreflopAggressor == hero)
                return;
            var preflopAggAction = PokerActionType.None;
            var otherRaise = 0;
            //var callAction = 0;
            var flopStartPot = -1.0f;
            var lastHeroAction = PokerActionType.None;

            foreach (var action in game.Actions)
            {
                if (action.Street != PokerStreet.Flop)
                    continue;
                if (!action.ActionType.IsGameAction())
                    continue;
                if (flopStartPot < 0.0f)
                    flopStartPot = action.Pot;
                var isCheck = action.ActionType == PokerActionType.Check;
                var isBet = action.ActionType == PokerActionType.Bet;
                var isCall = action.ActionType == PokerActionType.Call;
                var isRaise = action.ActionType == PokerActionType.Raise;
                var isFold = action.ActionType == PokerActionType.Fold;
                var flopType = GetActualFlopStats(preflopActions);

                if (action.Player == hero)
                {
                    var heroInPosToPreflopAggr = HeroInPos(hero.Position, preflopActions.PreflopAggressor.Position, game);
                    if (lastHeroAction == PokerActionType.None)
                    {
                        // * Игрок в позиции IP
                        if (heroInPosToPreflopAggr)
                        {
                            // Не было рейзов перед Hero
                            if (otherRaise == 0)
                            {
                                // PreflopAgg сделал чек
                                if (preflopAggAction == PokerActionType.Check)
                                {
                                    All.Bet_To_CheckPreflopAgg.Inc(isBet, hero.Position, heroInPosToPreflopAggr);
                                    flopType?.Bet_To_CheckPreflopAgg.Inc(isBet, hero.Position, heroInPosToPreflopAggr);

                                    All.Check_To_CheckPreflopAgg.Inc(isCheck, hero.Position, heroInPosToPreflopAggr);
                                    flopType?.Check_To_CheckPreflopAgg.Inc(isCheck, hero.Position, heroInPosToPreflopAggr);

                                    return;
                                }
                                // PreflopAgg сделал Bet
                                else if (preflopAggAction == PokerActionType.Bet)
                                {
                                    All.Fold_To_Cbet.Inc(isFold, hero.Position, heroInPosToPreflopAggr);
                                    flopType?.Fold_To_Cbet.Inc(isFold, hero.Position, heroInPosToPreflopAggr);

                                    All.Call_To_Cbet.Inc(isCall, hero.Position, heroInPosToPreflopAggr);
                                    flopType?.Call_To_Cbet.Inc(isCall, hero.Position, heroInPosToPreflopAggr);

                                    All.Raise_To_Cbet.Inc(isRaise, hero.Position, heroInPosToPreflopAggr);
                                    flopType?.Raise_To_Cbet.Inc(isRaise, hero.Position, heroInPosToPreflopAggr);
                                    return;
                                }
                            }
                        }
                        // * Игрок не в позиции OOP
                        else
                        {
                            // Не было рейзов перед Hero
                            if (otherRaise == 0)
                            {
                                if (action.Chips <= 0.2f * flopStartPot)
                                {
                                    All.DonkBetMin.Inc(isBet, hero.Position, heroInPosToPreflopAggr);
                                    flopType?.DonkBetMin.Inc(isBet, hero.Position, heroInPosToPreflopAggr);
                                }
                                All.DonkBet.Inc(isBet, hero.Position, heroInPosToPreflopAggr);
                                flopType?.DonkBet.Inc(isBet, hero.Position, heroInPosToPreflopAggr);
                                // Выходим после донк бета
                                if (isBet)
                                    return;
                            }
                        }
                    }
                    else
                    {
                        // Hero сделал Check
                        // Реакция не Cbet префлопп рейзера
                        if (otherRaise == 0 && preflopAggAction == PokerActionType.Bet && lastHeroAction == PokerActionType.Check)
                        {
                            All.Check_And_Fold_To_Cbet.Inc(isFold, hero.Position, false);
                            All.Check_And_Call_To_Cbet.Inc(isCall, hero.Position, false);
                            All.Check_And_Raise_To_Cbet.Inc(isRaise, hero.Position, false);

                            flopType?.Check_And_Raise_To_Cbet.Inc(isRaise, hero.Position, false);
                            flopType?.Check_And_Call_To_Cbet.Inc(isCall, hero.Position, false);
                            flopType?.Check_And_Raise_To_Cbet.Inc(isRaise, hero.Position, false);
                            return;
                        }
                    }
                    lastHeroAction = action.ActionType;
                }
                else
                {
                    if (preflopActions.PreflopAggressor.Position == action.Position)
                        preflopAggAction = action.ActionType;
                    else
                    if (action.ActionType == PokerActionType.Bet || action.ActionType == PokerActionType.Raise)
                        otherRaise++;
                }
            }
        }
    }

}
