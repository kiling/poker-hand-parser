﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PokerCore.Stats
{
    public class PostFlopStatsBase : StatsGroup
    {
        public StatsValue WTSD = null;
        public StatsValue WonSD = null;
        public StatsValue WonWhenSF = null;
        public StatsValue RaiseOrBet = null;
        public StatsValue Call = null;
        public StatsValue RaiseOrBet_Flop = null;
        public StatsValue Call_Flop = null;
        public StatsValue RaiseOrBet_Tern = null;
        public StatsValue Call_Tern = null;
        public StatsValue RaiseOrBet_River = null;
        public StatsValue Call_River = null;

        public float AgressionFactor()
        {
            if ((float)Call.Value == 0)
                return 0;
            return (float)RaiseOrBet.Value / (float)Call.Value;
        }

        public string AgressionFactor_Flop()
        {
            var data = (float)RaiseOrBet_Flop.Value / (float)Call_Flop.Value;
            return $"Af_flop {data.ToString("0.##")}/({RaiseOrBet_Flop.Possibly})";
        }

        public string AgressionFactor_Tern()
        {
            var data = (float)RaiseOrBet_Tern.Value / (float)Call_Tern.Value;
            return $"Af_tern {data.ToString("0.##")}/({RaiseOrBet_Tern.Possibly})";
        }

        public string AgressionFactor_River()
        {
            var data = (float)RaiseOrBet_River.Value / (float)Call_River.Value;
            return $"Af_river {data.ToString("0.##")}/({RaiseOrBet_River.Possibly})";
        }

        public PostFlopStatsBase() : base() 
        {
            Add(ref WTSD, "WTSD", "Насколько часто игрок добирается до шоудауна увидев флоп.");

            Add(ref WonSD, "WonSD", "Частота выигрыша на вскрытии если увидел флоп.");

            /*
            Средний WTSD (25-27%) и средний W$SD (51-55%) может означать как очень сильного 
            и в меру лузово-агрессивного игрока с уместными блефами, так и игрока, 
            не очень сильно понимающего постфлоп и иногда теряющегося по ходу раздачи. 
            Именно подобные показатели имеет большинство сильных игроков.

            Высокий WTSD (27-29%) и низкий W$SD (46-51%) вашего оппонента означает, 
            что он часто доходит до шоудауна и проигрывает на нем, не имея сильной руки. Т
            акже это может означать, что он может неуместно блефовать на не очень подходящих досках, 
            либо просто блефовать где попало (чем ниже W$SD, тем хуже он блефует).
             */

            Add(ref WonWhenSF, "WonWhenSF", "Показывает как часто игрок выигрывает пот, после того как увидел флоп.");
            /*
             Если W$WSF < 40 , то его обладатель блефует раз в год. Если у вашего оппа W$WSF > 48, 
            то он будет блефовать при каждом удобном случае. W$SD - показывает как часто опп дойдя 
            до вскрытия выиграл банк. 51% - средний показатель для W$SD (no small pots) 
             */

            Add(ref RaiseOrBet, "RaiseOrBet", "Количество Raise или Bet на постфлопе");
            Add(ref Call, "Call", "Количество Call на постфлопе");

            Add(ref RaiseOrBet_Flop, "RaiseOrBet_Flop", "Количество Raise или Bet на флопе");
            Add(ref Call_Flop, "Call_Flop", "Количество Call на постфлопе");

            Add(ref RaiseOrBet_Tern, "RaiseOrBet_Tern", "Количество Raise или Bet на терне");
            Add(ref Call_Tern, "Call_Tern", "Количество Call на постфлопе");

            Add(ref RaiseOrBet_River, "RaiseOrBet_River", "Количество Raise или Bet на ривере");
            Add(ref Call_River, "Call_River", "Количество Call на ривере");
        }
    }

    // OneRaisePot_HeadsUp
    // OneRaisePot_MultiPot
    // 3BetPot_MultiPot
    public enum PreflopActionPotType { LimpPot, RaisePot, Bet3Pot, Bet4Pot };

    public class PreflopActionStatsType
    {
        public PreflopActionPotType PotType { get; private set; }
        public Player PreflopAggressor { get; private set; } = null;
        public int PlayersInGame { get; private set; } = 0;
        public List<Player> Players { get; private set; } = new List<Player>();
        public bool PreflopAggInPos { get; private set; } = false;
        public PreflopActionStatsType(int preflopCountRaise, List<Player> playersInGame, Player preflopAggressor, bool preflopAggInPos)
        {
            if (preflopCountRaise == 0)
                PotType = PreflopActionPotType.LimpPot;
            else if (preflopCountRaise == 1)
                PotType = PreflopActionPotType.RaisePot;
            else if (preflopCountRaise == 2)
                PotType = PreflopActionPotType.Bet3Pot;
            else if (preflopCountRaise > 3)
                PotType = PreflopActionPotType.Bet4Pot;
            PreflopAggInPos = preflopAggInPos;
            PreflopAggressor = preflopAggressor;
            PlayersInGame = playersInGame.Count;
            Players.AddRange(playersInGame);
            if (PlayersInGame < 2)
                throw new System.Exception("Не верное количество игроков!");
        }
    }

    public class PostFlopStats : StatsGroup
    {
        public static PreflopActionStatsType GetPreflopActions(PokerGame game)
        {
            var countRaise = 0;
            Player preflopAgg = null;

            var players = new List<Player>();
            foreach (var player in game.PlayersAtTable)
                players.Add(player);

            foreach (var action in game.Actions)
            {
                if (action.Street == PokerStreet.Preflop)
                {
                    if (action.ActionType.IsBlind())
                        continue;

                    if (action.ActionType == PokerActionType.Raise)
                    {
                        countRaise++;
                        preflopAgg = action.Player;
                    }
                    else if (action.ActionType == PokerActionType.Fold)
                        players.Remove(action.Player);
                }
                else
                    break;
            }

            if (players.Count < 2)
                return null;

            var preflopAggInPos = false;
            if (preflopAgg != null)
            {
                preflopAggInPos = true;
                foreach (var player in players)
                    if (player != preflopAgg && !HeroInPos(preflopAgg.Position, player.Position, game))
                    {
                        preflopAggInPos = false;
                        break;
                    }
            }

            return new PreflopActionStatsType(countRaise, players, preflopAgg, preflopAggInPos); ;
        }
        public PostFlopStatsBase All { get; private set; } = null;
        public PostFlopStatsBase OneRaisePot_HeadsUp { get; private set; } = null;
        public PostFlopStatsBase OneRaisePot_MultiPot { get; private set; } = null;
        public PostFlopStatsBase Bet3Pot { get; private set; } = null;
        public PostFlopStatsBase Bet4Pot { get; private set; } = null;
         

        public override void StatsGrabber(PokerGame game, Player hero)
        {
            AggFactor(game, hero);
            WTSDAction(game, hero);
        }

        public PostFlopStats() : base()
        {
            if (All == null)
                All = new PostFlopStatsBase();
            All.Name = "All";
            Groups.Add(All);

            if (OneRaisePot_HeadsUp == null)
                OneRaisePot_HeadsUp = new PostFlopStatsBase();
            OneRaisePot_HeadsUp.Name = "OneRaisePot_HeadsUp";
            Groups.Add(OneRaisePot_HeadsUp);

            if (OneRaisePot_MultiPot == null)
                OneRaisePot_MultiPot = new PostFlopStatsBase();
            OneRaisePot_MultiPot.Name = "OneRaisePot_MultiPot";
            Groups.Add(OneRaisePot_MultiPot);

            if (Bet3Pot == null)
                Bet3Pot = new PostFlopStatsBase();
            Bet3Pot.Name = "Bet3Pot";
            Groups.Add(Bet3Pot);

            if (Bet4Pot == null)
                Bet4Pot = new PostFlopStatsBase();
            Bet4Pot.Name = "Bet4Pot";
            Groups.Add(Bet4Pot);
        }

        private PostFlopStatsBase GetActualStats(PreflopActionStatsType preflopActions)
        {
            PostFlopStatsBase type = null;
            if (preflopActions.PotType == PreflopActionPotType.RaisePot)
            {
                if (preflopActions.PlayersInGame == 2)
                    type = OneRaisePot_HeadsUp;
                else
                    type = OneRaisePot_MultiPot;
            }
            else if (preflopActions.PotType == PreflopActionPotType.Bet3Pot)
                type = Bet3Pot;
            else if (preflopActions.PotType == PreflopActionPotType.Bet4Pot)
                type = Bet4Pot;
            return type;
        }

        private void AggFactor(PokerGame game, Player hero)
        {
            var preflopType = GetPreflopActions(game);
            if (preflopType == null)
                return;
            var statsType = GetActualStats(preflopType);

            foreach (var action in game.Actions)
            {
                if (!action.ActionType.IsGameAction())
                    continue;
                if (action.Street != PokerStreet.Preflop && action.Player == hero)
                {
                    var isRaise = action.ActionType == PokerActionType.Bet || action.ActionType == PokerActionType.Raise;
                    var isCall = action.ActionType == PokerActionType.Call;

                    All.RaiseOrBet.Inc(isRaise, hero.Position, preflopType.PreflopAggInPos);
                    All.Call.Inc(isCall, hero.Position, preflopType.PreflopAggInPos);
                    statsType?.RaiseOrBet.Inc(isRaise, hero.Position, preflopType.PreflopAggInPos);
                    statsType?.Call.Inc(isCall, hero.Position, preflopType.PreflopAggInPos);

                    if (action.Street == PokerStreet.Flop)
                    {
                        All.RaiseOrBet_Flop.Inc(isRaise, hero.Position, preflopType.PreflopAggInPos);
                        All.Call_Flop.Inc(isCall, hero.Position, preflopType.PreflopAggInPos);
                        statsType?.RaiseOrBet_Flop.Inc(isRaise, hero.Position, preflopType.PreflopAggInPos);
                        statsType?.Call_Flop.Inc(isCall, hero.Position, preflopType.PreflopAggInPos);
                    }
                    else if (action.Street == PokerStreet.Turn)
                    {
                        All.RaiseOrBet_Tern.Inc(isRaise, hero.Position, preflopType.PreflopAggInPos);
                        All.Call_Tern.Inc(isCall, hero.Position, preflopType.PreflopAggInPos);
                        statsType?.RaiseOrBet_Tern.Inc(isRaise, hero.Position, preflopType.PreflopAggInPos);
                        statsType?.Call_Tern.Inc(isCall, hero.Position, preflopType.PreflopAggInPos);

                    }
                    else if (action.Street == PokerStreet.River)
                    {
                        All.RaiseOrBet_River.Inc(isRaise, hero.Position, preflopType.PreflopAggInPos);
                        All.Call_River.Inc(isCall, hero.Position, preflopType.PreflopAggInPos);
                        statsType?.RaiseOrBet_River.Inc(isRaise, hero.Position, preflopType.PreflopAggInPos);
                        statsType?.Call_River.Inc(isCall, hero.Position, preflopType.PreflopAggInPos);
                    }
                }
            }
        }

        private void WTSDAction(PokerGame game, Player hero)
        {
            var preflopType = GetPreflopActions(game);
            if (preflopType == null)
                return;
            var statsType = GetActualStats(preflopType);
            var isWTSD = false;
            var isWin = false;
            var haveActionInFlop = false;
            var countWin = 0;
            foreach (var action in game.Actions)
            {
                if (action.Street == PokerStreet.Flop)
                {
                    if (action.Player == hero && action.ActionType.IsGameAction())
                        haveActionInFlop = true;
                }
                // else
                if (action.Player == hero && haveActionInFlop)
                {
                    if (action.Street == PokerStreet.ShowDown &&
                        (action.ActionType == PokerActionType.ShowHand || action.ActionType == PokerActionType.MucksHand))
                        isWTSD = true;
                    if (action.ActionType == PokerActionType.Wins || action.ActionType == PokerActionType.Wins_Side_Pot)
                        isWin = true;
                }
                if (action.ActionType == PokerActionType.Wins)
                    countWin++;
            }
            if (countWin > 1)
                isWin = false;
            if (haveActionInFlop)
            {
                All.WTSD.Inc(isWTSD, hero.Position, preflopType.PreflopAggInPos);
                statsType?.WTSD.Inc(isWTSD, hero.Position, preflopType.PreflopAggInPos);

                if (isWTSD)
                {
                    All.WonSD.Inc(isWin, hero.Position, preflopType.PreflopAggInPos);
                    statsType?.WonSD.Inc(isWin, hero.Position, preflopType.PreflopAggInPos);
                }

                All.WonWhenSF.Inc(isWin, hero.Position, preflopType.PreflopAggInPos);
                statsType?.WonWhenSF.Inc(isWin, hero.Position, preflopType.PreflopAggInPos);
            }
        }
    }
}
