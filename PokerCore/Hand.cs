﻿using System;

namespace PokerCore
{
    public class HandException : Exception
    {
        public HandException(string message) : base(message)
        {
        }
    }

    /// <summary>
    /// Класс описывающий руку
    /// </summary>
    public class Hand
    {
        public Hand(string str)
        {
            if (str.Length == 4)
            {
                Card1 = new Card(str[0].ToString() + str[1].ToString());
                Card2 = new Card(str[2].ToString() + str[3].ToString());
                if (Card2 > Card1)
                {
                    var card = Card1;
                    Card1 = Card2;
                    Card2 = card;
                }
            }
            else
                throw new HandException($"Error parse hand from {str}");
        }

        /// <summary>
        /// Руку в текстовый вид AsKh
        /// </summary>
        public override string ToString()
        {
            return Card1.ToString() + Card2.ToString();
        }

        /// <summary>
        /// Первая карта
        /// </summary>
        public Card Card1 { get; private set; } = null;

        /// <summary>
        /// Вторая карта
        /// </summary>
        public Card Card2 { get; private set; } = null;
    }
}
